import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationUrls } from 'src/app/Core/Models/application-urls';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminuserService {

  constructor(private http: HttpClient) { }

  GetUserListByStatus(status): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_USERS_LIST_BY_STATUS + '/' + status;
    return this.http.get(url);
  }

  UserApprovalStatus(formdata): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_USER_APPROVAL_STATUS;
    return this.http.post(url, formdata);
  }
}
