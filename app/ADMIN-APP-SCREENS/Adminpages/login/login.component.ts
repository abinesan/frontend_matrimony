import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Service-Layer/user.service';
import { AdminUserService } from 'src/app/Service-Layer/Admin-Service-Layer/admin-user.service';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  admin_email: any;
  admin_password; any;
  constructor(private router: Router, private userservice: UserService,
              private adminservice: AdminUserService,
              private messageService: MessageService) { }

  ngOnInit() {
  }

  AdminLogin(adminformdata) {    this.clearMessage();
       const formdata = {
      email: adminformdata.txt_user_email,
      password: adminformdata.txt_user_password

    };
       this.adminservice.AdminUserLogin(formdata).subscribe(data => {
      if (data.status == 'success') {        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });

                                             this.userservice.setUserRole('admin');
                                             this.redirectToPage();

      } else if (data.status == 'failure') {        console.log('Authentication Failure');
                                                  this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }
    },
    err => {
      console.log('Sommething went wrong while login');
      this.messageService.add({ severity: 'error', summary: 'Sommething went wrong while Admin login', detail: err });
      console.log(err);

    });
  }

  clearMessage() {
    this.messageService.clear();
  }
  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/admin/userapproval']);
    }, 2000);
  }

}
