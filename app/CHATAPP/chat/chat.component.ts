import { Component, OnInit } from '@angular/core';
import { MatchesService } from 'src/app/Service-Layer/matches.service';
import { UserService } from 'src/app/Service-Layer/user.service';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.less']
})
export class ChatComponent implements OnInit {

  online_user_details: any;
  display_chat_window = false;
  selected_reciverId: number;
  Loggedin_id: number;
  Current_chatRoom_id: number;
  msg_conversation_list: any;
  new_message_status = '';
  latest_msg_count: number;
  searchText: string;
  selected_personName: string;
  selected_personimage: string;
  is_chat_person_selected = false;
  My_message_list: any;
  Their_message_list: any;
  displaychat_box = false;
  displaychatsection = true;
  new_online_user_details: any;
  old_online_user_details: any;
  all_chat_user_details: any;
  typemessage: any;
  intervalId: any;

  displayChatLoader = false;
  users: any;
  constructor(private matchesservice: MatchesService, private userservice: UserService, private chatservice: ChatService) { }

  ngOnInit() {
   // this.displayProfileUsers();
   this.displayNewChatUsers();
   this.displayOldChatUsers();
   // this.chatservice.castchat.subscribe(chatlist => this.msg_conversation_list = chatlist);
   // this.getMessagesList(this.Current_chatRoom_id);

   // this.getMessagesList();
  }
  // displayProfileUsers() {
  //   this.matchesservice.GetTodayMatchesProfileData(this.userservice.getLoggedinUserId()).subscribe(data => {
  //     this.online_user_details = data.myMatchesList;
  //     this.Loggedin_id = parseInt(this.userservice.getLoggedinUserId());

  //   })
  // }

  displayNewChatUsers() {
    this.chatservice.GetNewChatUserList(parseInt(this.userservice.getLoggedinUserId())).subscribe(data => {
      if (data.status == 'success') {
      this.new_online_user_details = data.userList;
      this.Loggedin_id = parseInt(this.userservice.getLoggedinUserId());
      } else {
        this.new_online_user_details = null;
      }

    });
  }
  displayOldChatUsers() {
    this.chatservice.GetOldChatUserList(parseInt(this.userservice.getLoggedinUserId())).subscribe(data => {
      if (data.status == 'success') {
        this.old_online_user_details = data.userList;
        this.Loggedin_id = parseInt(this.userservice.getLoggedinUserId());
      } else {
        this.old_online_user_details = null;
      }

    });
  }
  showchatbox() {
    this.all_chat_user_details = this.new_online_user_details;
    if (this.displaychat_box) {
      this.displaychat_box = false;
    } else {
      this.displaychat_box = true;

    }
  }
  NewChatUsers() {
     this.all_chat_user_details = this.new_online_user_details;
  }
  OldChatUsers() {
     this.all_chat_user_details = this.old_online_user_details;
  }

  getChatRoom(reciver_name, reciver_img, reciverId) {
    if (reciver_img != null) {
      reciver_img = reciver_img[0];
    }    this.is_chat_person_selected = true;
    this.selected_personName = reciver_name;
    this.selected_personimage = reciver_img;
    this.selected_reciverId = parseInt(reciverId);
    const userinfo = {
      senderId:  parseInt(this.userservice.getLoggedinUserId()),
      receiverId: parseInt(reciverId)
    };
    this.chatservice.getChatRoomID(userinfo).subscribe(data => {
      this.display_chat_window = true;
      this.displaychatsection = true;
      if (data.status == 'success') {        this.Current_chatRoom_id = parseInt(data.chatId);
               this.selected_reciverId = parseInt(reciverId);
               console.log('ChatId :' + this.Current_chatRoom_id);
               this.getMessagesList(this.Current_chatRoom_id);

      }
    });

  }

  getMessagesList(selected_chatid) {    this.displayChatLoader = true;
                                       const chatinfo = {
        chatId: parseInt(selected_chatid),
        message_count: 1
      };

                                       this.chatservice.getMessagesListByChatID(chatinfo).subscribe(data => {


      // loadind the data if success
      if (data.status == 'success') {
        debugger;
        this.msg_conversation_list = data;
        this.displayChatLoader = false;

       // this.selected_reciverId = parseInt(selected_chatid);
      } else {        this.msg_conversation_list = null;
               this.displayChatLoader = false;
        }

        // this.My_message_list = data.chatDetails.filter(x => x.senderId == this.userservice.getLoggedinUserId());
        // this.Their_message_list=data.chatDetails.filter(x => x.senderId == 7);//data.chatDetails.filter(x => x.senderId == this.selected_reciverId);
        // this.latest_msg_count = data.chatDetails.length;
    }, error => {

    }, () => {

    });
    // cal the chat list for every 8 seconds

      // this.intervalId = setInterval(() => this.getMessagesList(this.Current_chatRoom_id), 5000);



  }
  refreshchatwindow() {
    this.getMessagesList(this.Current_chatRoom_id);
  }

  addNewMessage(new_msg) {
   // this.displayChatLoader = true;
    const msginfo = {
      senderId: parseInt(this.userservice.getLoggedinUserId()),
      chatId: this.Current_chatRoom_id,
      message: new_msg,
      message_count: 1
    };
    this.chatservice.SendNewMessage(msginfo).subscribe(data => {
      if (data.status == 'success') {
       // this.new_message_status = data.message;
        this.getMessagesList(this.Current_chatRoom_id);

        this.typemessage = '';
       // this.chatservice.sendChat(this.Current_chatRoom_id);
        // this.displayChatLoader = true;
      } else {
        this.new_message_status = 'Failed to Send the Message';
      }
    });
  }
  removeleftchatwindow() {
      if (this.displaychatsection) {
        this.displaychatsection = false;

       //  clearTimeout(this.intervalId)
      } else {
        this.displaychatsection = true;

      }
  }


}
