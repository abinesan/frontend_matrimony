import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler } from '@angular/common/http';
import { UserService } from 'src/app/Service-Layer/user.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TokenInteceptorService implements HttpInterceptor {

  constructor(private injector: Injector) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userservice = this.injector.get(UserService);
    const final_token = userservice.getAuthenticationTokens();
    const modifiedReq = req.clone({
      // use always ts code style to refer variables
   // headers: req.headers.set('Access-Token', `Bearer ${access_token}`).set('Refresh-Token', `Bearer ${refresh_token}`)
      headers: req.headers.set('Authorization', `${final_token}`)
    });
    return next.handle(modifiedReq);

  }

}

