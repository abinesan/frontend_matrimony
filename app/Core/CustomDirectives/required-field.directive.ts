import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[required-astrick]'
})
export class RequiredFieldDirective implements OnInit {

  constructor(private el:ElementRef,private renderer: Renderer2) { }

  ngOnInit() {
    const parent = this.renderer.parentNode(this.el.nativeElement);
    parent.getElementsByTagName('label')[0].innerHTML += '<span style="color:red; font-weight: bold;margin-left: 3px; font-size: 1.2em;">*</span>';
    
  }
}
