import { Injectable } from '@angular/core';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InboxService {

  constructor(private http: HttpClient) { }

  DisplayInterestSentList(loggedin_user_id): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_INTEREST_SENT_LIST + '/' + loggedin_user_id;
    return this.http.get(url);

  }
  DisplayInterestReceivedList(loggedin_user_id): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_INTEREST_RECEIVED_LIST + '/' + loggedin_user_id;
    return this.http.get(url);
  }
  DisplayInterestAcceptedList(loggedin_user_id): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_INTEREST_ACCEPT_LIST + '/' + loggedin_user_id;
    return this.http.get(url);
  }
  DisplayInterestRejectedList(loggedin_user_id): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_INTEREST_REJECTED_LIST + '/' + loggedin_user_id;
    return this.http.get(url);
  }
}
