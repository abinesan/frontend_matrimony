import { Injectable } from '@angular/core';
import { BnNgIdleService } from 'bn-ng-idle'; // import it to your component
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
@Injectable({
  providedIn: 'root'
})
export class TimeoutService {

  constructor(private bnIdle: BnNgIdleService,
              private router: Router,
              private messageService: MessageService) { }

  Idletimeout() {
    // time will be in seconds
    this.bnIdle.startWatching(1500).subscribe((isTimedOut: boolean) => {
      if (isTimedOut) {
        this.messageService.add({ severity: 'info', summary: 'your session expired.navigating to login page again..', detail: ' ' });
      //  alert('your session expired.navigating to login page again..');
        this.bnIdle.stopTimer();
        this.redirectToPage();

      }

    });
  }
  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/signup']);
    }, 2000);
  }
}
