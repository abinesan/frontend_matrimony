import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserprofileService {

  constructor(private http: HttpClient) { }

  UserProfileUpdate(updateprofile): Observable<any> {

    // let headers = new HttpHeaders({ 'Access-Token': 'wT7oRi6zkr8URXUCVrSp7F4R1B9eV9w', 'userid':'4'});
    // headers.append("Access-Control-Allow-Origin", "*");
    // headers.append("Access-Control-Allow-Credentials", "true");
    // headers.append("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    // headers.append("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    // let options = { headers: headers };

    const url = ApplicationUrls.BASE_URL + ApplicationUrls.USER_PROFILE_URL;
    return this.http.post(url, updateprofile);
  }

  GetCountryList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_ALLCOUNTRIES_LIST;
    return this.http.get(url);
  }

  GetStateListByCountryID(select_country_id): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_STATELIST_BY_COUNTRY_ID + '/' + select_country_id;
    return this.http.get(url);
  }

  GetCityListByStateID(selected_state_id): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_CITYLIST_BY_STATE_ID + '/' + selected_state_id;
    return this.http.get(url);
  }

  GetUserProfileDataByUserID(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_USER_PROFILE_DATA_BY_USERID + '/' + current_userid;
    return this.http.get(url);
  }
  PartnerProfileUpdate(updatepartnerprofile): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.PARTNER_PROFILE_URL;
    return this.http.post(url, updatepartnerprofile);
  }

  GetPartnerProfileDataByUserID(current_userId): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_PARTNER_PROFILE_DATA_BY_USERID + '/' + current_userId;
    return this.http.get(url);
  }
  GetCasteList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_CASTE_LIST;
    return this.http.get(url);
  }
  GetPartnerOccupationList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_PARTNER_OCCUPATION_LIST;
    return this.http.get(url);
  }
  GetPartnerEducationList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_PARTNER_EDUCATION_LIST;
    return this.http.get(url);
  }
  GetMotherTongueList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_MOTHER_TONGUE_LIST;
    return this.http.get(url);
  }

  GetRasiList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_RASI_LIST;
    return this.http.get(url);
  }

  GetNakshatraList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_NAKSHATRA_LIST;
    return this.http.get(url);
  }

  GetGothramList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_GOTHRAM_LIST;
    return this.http.get(url);
  }
  GetPartnerStateListByCountryIDS(country_id_list): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_PARTNER_STATELIST_BY_COUNTRY_IDS;
    return this.http.post(url, country_id_list);
  }
  GetPartnerCityListByStateIDS(state_id_list): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_PARTNER_CITYLIST_BY_STATE_IDS;
    return this.http.post(url, state_id_list);
  }


  ContactFormUpdate(contactform_update): Observable<any> {
    // const httpOptions = {
    // headers: new HttpHeaders({
    //     Access: localStorage.getItem('accessToken'),
    //   Refresh: localStorage.getItem('refreshToken'),
    //   // 'Access-Control-Allow-Origin': "*",
    //   'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
    //   })
    // };
    // let headersToSend = new HttpHeaders();
    // headersToSend = headersToSend
    // .set('refresh-token', 'vijay')
    //   .set('access-token', 'raju')
    //   .set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_CONTACT_ADD_UPDATE_URL;
      return this.http.post(url, contactform_update);
      // return this.http.post(url, contactform_update, httpOptions);
  }
  GetContactDetailsByUserId(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_CONTACT_DETAILS_BY_USERID + '/' + current_userid;
    return this.http.get(url);
  }

  ProfileRightsFormUpdate(profileform): Observable<any> {
   
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_PROFILE_RIGHTS_ADD_UPDATE_URL;
      return this.http.post(url, profileform);
   
  }
}
