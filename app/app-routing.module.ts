import { ViewprofileComponent } from './dashboard/viewprofile/viewprofile.component';
import { PartnerPreferenceComponent } from './dashboard/partner-preference/partner-preference.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { WhychooseComponent } from './whychoose/whychoose.component';
import { FooterComponent } from './footer/footer.component';
import { CarouselComponent } from './carousel/carousel.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { RegisterComponent } from './signup/register/register.component';
import { MatchesComponent } from './matches/matches.component';
import { PricingComponent } from './pricing/pricing.component';
import { SearchComponent } from './search/search.component';
import { InboxComponent } from './inbox/inbox.component';
import { EditProfileComponent } from './dashboard/edit-profile/edit-profile.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ViewfullprofileComponent } from './matches/viewfullprofile/viewfullprofile.component';
import { DummyMatchesComponent } from './dummy-matches/dummy-matches.component';
import { AuthGuard } from './Core/Guards/auth.guard';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ChatComponent } from './CHATAPP/chat/chat.component';
import { LoginComponent } from './ADMIN-APP-SCREENS/Adminpages/login/login.component';
import { AdminGuard } from './Core/Guards/admin.guard';
import { RegistrationFormComponent } from './signup/registration-form/registration-form.component';
import { AdminDashboardComponent } from './ADMIN-APP-SCREENS/Adminpages/admin-dashboard/admin-dashboard.component';
import { UserapprovalComponent } from './ADMIN-APP-SCREENS/Adminpages/userapproval/userapproval.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { NewPasswordComponent } from './forgot-password/new-password/new-password.component';
import { ManageadsComponent } from './ADMIN-APP-SCREENS/Adminpages/manageads/manageads.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home',  component: HomeComponent},
  { path: 'dashboard',  component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'signup',  component: SignupComponent},
  { path: 'header',  component: HeaderComponent},
  { path: 'about',  component: AboutComponent},
  { path: 'whychoose',  component: WhychooseComponent},
  { path: 'footer',  component: FooterComponent},
  { path: 'carousel',  component: CarouselComponent},
  { path: 'register',  component: RegisterComponent},
  { path: 'matches',  component: MatchesComponent, canActivate: [AuthGuard]},
  { path: 'viewfullprofile',  component: ViewfullprofileComponent, canActivate: [AuthGuard]},
  { path: 'search',  component: SearchComponent, canActivate: [AuthGuard]},
  { path: 'inbox',  component: InboxComponent, canActivate: [AuthGuard]},
  { path: 'price',  component: PricingComponent, canActivate: [AuthGuard]},
  { path: 'editprofile',  component: EditProfileComponent, canActivate: [AuthGuard]},
  { path: 'forgot-password',  component: ForgotPasswordComponent},
  { path: 'partnerpreferecnce',  component: PartnerPreferenceComponent, canActivate: [AuthGuard]},
  { path: 'viewprofile', component: ViewprofileComponent, canActivate: [AuthGuard]},
  { path: 'privacy', component: PrivacyPolicyComponent},
  { path: 'register-form', component: RegistrationFormComponent},
  { path: 'reset-password', component: ResetPasswordComponent},
  { path: 'new-password', component: NewPasswordComponent},
  { path: 'viewfullprofilewithId/:CardId',  component: ViewfullprofileComponent,canActivate:[AuthGuard]},


  { path: 'terms&conditions', component:   TermsAndConditionsComponent},
  { path: 'dummy', component: DummyMatchesComponent},
  { path: 'chatroom', component: ChatComponent },
  {path: 'admin', component: LoginComponent},
  {path: 'admin/admin-dashboard', component: AdminDashboardComponent, canActivate: [AuthGuard]},
  { path: 'admin/userapproval', component: UserapprovalComponent , canActivate: [AdminGuard]},
  {path: 'admin/manageads', component: ManageadsComponent, canActivate: [AdminGuard]}


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
