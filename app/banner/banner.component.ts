import { Component, OnInit } from '@angular/core';
import { UserService } from '../Service-Layer/user.service';
import { Router } from '@angular/router';
import { Message } from 'primeng//api';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.less']
})
export class BannerComponent implements OnInit {
  listofprofiles = ['Self', 'Son', 'Daughter', 'Brother', 'Sister', 'Friend', 'Relative'];


  constructor(private userservice: UserService, private route: Router, private messageService: MessageService) { }

  ngOnInit() {
  }
  clearMessage() {
    this.messageService.clear();
  }


  GetUserBasicDetails(UserBasicDetails) {
    debugger;
    this.clearMessage();

    if (UserBasicDetails.ddl_profile_select == 'Son' || UserBasicDetails.ddl_profile_select == 'Brother') {
      UserBasicDetails.txt_looking_for = 'Female';
    } else if (UserBasicDetails.ddl_profile_select == 'Daughter' || UserBasicDetails.ddl_profile_select == 'Sister') {
      UserBasicDetails.txt_looking_for = 'Male';
    }
    if (UserBasicDetails.txt_reg_username == '' || UserBasicDetails.ddl_countryCode == '' ||
      UserBasicDetails.txt_reg_user_mobileno == '' || UserBasicDetails.ddl_profile_select == '' ||
      UserBasicDetails.txt_looking_for == '') {
      this.messageService.add({ severity: 'warn', summary: 'All fields are required', detail: '' });
      return false;
    }
    if (UserBasicDetails.txt_reg_user_mobileno.length < 10) {
      this.messageService.add({ severity: 'warn', summary: 'Mobile number should be 10 digits', detail: '' });
      return false;
    } else {

      const basicdetails = {
        fullName: UserBasicDetails.txt_reg_username,
        mobileCountryCode: '+' + UserBasicDetails.ddl_countryCode,
        mobileNumber: UserBasicDetails.txt_reg_user_mobileno,
        create_profile_for: UserBasicDetails.ddl_profile_select,
        I_am_looking_for: UserBasicDetails.txt_looking_for


      };
      this.userservice.setUserBasicDetails(basicdetails);

      const proper_mobileno = '+' + UserBasicDetails.ddl_countryCode + UserBasicDetails.txt_reg_user_mobileno;
      this.userservice.setUserMobileNumber(proper_mobileno);
      this.route.navigate(['/register-form']);

    }
  }

}
