import { Component, OnInit, ViewChild } from '@angular/core';
import { TimeoutService } from '../Service-Layer/timeout.service';
import { BnNgIdleService } from 'bn-ng-idle'; // import it to your component
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { DashboardService } from '../Service-Layer/dashboard.service';
import { UserService } from '../Service-Layer/user.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { MatchesService } from '../Service-Layer/matches.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  all_dashboard_info: any;
  ismyprofilevisited: any;

  all_viewed_profiles_byme_list: any;
  all_viewed_profiles_byothers_list: any;
  displayedColumns: string[] = ['ProjectName', 'Customer', 'Status', 'MilestoneData', 'ProjectType', 'ProImg', 'ProjectManagar', 'SalesContact', 'HoursSpent', 'ActiveResources'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


  constructor(private dashboardservice: DashboardService, private userservice: UserService,
    private matchservice: MatchesService, private router: Router,
    private messageService: MessageService) {

  }
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.DisplayDashboardInfo();
    this.DisplayViewedprofileListByOthers();
    this.DisplayViewedProfileListByMe();
  }
  rejectprofile(selected_profile_id, user_interest) {
    this.clearMessage();
    const formdata = {
      senderId: this.userservice.getLoggedinUserId(),
      receiverId: selected_profile_id,
      connectionStatus: user_interest
    };
    this.matchservice.SendAccept(formdata).subscribe(data => {
      if (data.status == 'success') {
        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
        this.DisplayViewedprofileListByOthers();
        this.DisplayViewedProfileListByMe();


      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }
    });
  }

  DisplayDashboardInfo() {
    this.dashboardservice.getdashboardList(this.userservice.getLoggedinUserId()).subscribe(data => {
      this.all_dashboard_info = data;
    });
  }

  DisplayViewedprofileListByOthers() {
    this.matchservice.GetRecentlyViewedProfileListByOthers(this.userservice.getLoggedinUserId()).subscribe(data => {
      if (data.status == 'success') {
        this.all_viewed_profiles_byothers_list = data.userList;
      }
    })
  }
  DisplayViewedProfileListByMe() {
    this.matchservice.GetRecentlyViewedProfileListByMe(this.userservice.getLoggedinUserId()).subscribe(data => {
      if (data.status == 'success') {
        this.all_viewed_profiles_byme_list = data.userList;
      }
    })
  }
  gotoPartnerProfilePage() {
    this.router.navigate(['/editprofile']);
  }

  viewmoredetails(visited_profileid, current_image_path, current_picture_rights) {
    debugger;

    // changing the value of is my profile visted value if we cliked on more button on UI
    this.ismyprofilevisited = 1;

    this.matchservice.GetFullProfileViewData({
      senderId: parseInt(this.userservice.getLoggedinUserId()),
      receiverId: parseInt(visited_profileid),
      profile_view_status: this.ismyprofilevisited
    }).subscribe(data => {
      // setting the image
      this.matchservice.SetCurrentiamge(current_image_path);
      this.matchservice.SetCurrentImageRights(current_picture_rights);
      this.router.navigate(['/viewfullprofilewithId', visited_profileid]);
    });
  }

  clearMessage() {
    this.messageService.clear();
  }


}


export interface PeriodicElement {
  ProjectName: string;
  Customer: string;
  Status: string;
  MilestoneData: string;
  ProjectType: string;
  ProjectManagar: string;
  SalesContact: string;
  HoursSpent: string;
  ActiveResources: string;
  ProImg: string;

}

const ELEMENT_DATA: PeriodicElement[] = [

  { ProjectName: 'Project A', Customer: 'TJX', Status: 'Planning', MilestoneData: 'Sprint 2', ProjectType: 'Commercial', ProjectManagar: 'Philip', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '200 Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project B', Customer: 'DB', Status: 'Under Dev', MilestoneData: 'Sprint 6', ProjectType: 'R&D', ProjectManagar: 'Scott', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '17k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project C', Customer: 'e-Boks', Status: 'Testing', MilestoneData: 'Sprint 3', ProjectType: 'Commercial', ProjectManagar: 'Nick', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '33k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project D', Customer: 'CHUMS', Status: 'Inproduction', MilestoneData: 'Sprint 6', ProjectType: 'Product Dev', ProjectManagar: 'Christi', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '30k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project E', Customer: 'China Lake', Status: 'Planning', MilestoneData: 'Sprint 2', ProjectType: 'Internal', ProjectManagar: 'Luo', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '140 Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project F', Customer: 'NGC', Status: 'Testing', MilestoneData: 'Sprint 1', ProjectType: 'Commercial', ProjectManagar: 'Philip', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '40k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project G', Customer: 'VA MUMPS', Status: 'Under Dev', MilestoneData: 'Sprint 4', ProjectType: 'R&D', ProjectManagar: 'Scott', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '28k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project H', Customer: 'TJX', Status: 'Inproduction', MilestoneData: 'Sprint 3', ProjectType: 'Commercial', ProjectManagar: 'Nick', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '42k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project I', Customer: 'DB', Status: 'Inproduction', MilestoneData: 'Sprint 1', ProjectType: 'Internal', ProjectManagar: 'Christi', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '38k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project J', Customer: 'e-Boks', Status: 'Testing', MilestoneData: 'Sprint 3', ProjectType: 'R&D', ProjectManagar: 'Luo', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '29k Hrs', ActiveResources: 'Hydrogen' },

  { ProjectName: 'Project K', Customer: 'NGC', Status: 'Testing', MilestoneData: 'Sprint 5', ProjectType: 'Internal', ProjectManagar: 'Nick', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '20k Hrs', ActiveResources: 'Hydrogen' },

];
