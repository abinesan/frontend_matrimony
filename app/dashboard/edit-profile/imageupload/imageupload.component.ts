import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imageupload',
  templateUrl: './imageupload.component.html',
  styleUrls: ['./imageupload.component.less']
})
export class ImageuploadComponent implements OnInit {
  selected_image_names = [];
  urls = [];

  constructor() { }

  ngOnInit() {
  }

  onSelectFile(event) {    if (event.target.files && event.target.files[0]) {
      let totalFiles = event.target.files.length;
      for (let i = 0; i < totalFiles; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(event.target.files[i]);
        // console.log(event.target.files[i]);
        this.selected_image_names.push(event.target.files[i].name);
      }
    }
  }
  removeImage(selected_preview_image) {
    this.urls.splice(selected_preview_image, 1);
  }
}
