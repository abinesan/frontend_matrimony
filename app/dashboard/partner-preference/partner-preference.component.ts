import { Component, OnInit } from '@angular/core';
import { UserprofileService } from 'src/app/Service-Layer/userprofile.service';
import { UserService } from 'src/app/Service-Layer/user.service';

@Component({
  selector: 'app-partner-preference',
  templateUrl: './partner-preference.component.html',
  styleUrls: ['./partner-preference.component.less']
})
export class PartnerPreferenceComponent implements OnInit {
partnerprofiledetails: any;
  constructor(private userprofileservice: UserprofileService,
              private userservice: UserService) { }

  ngOnInit() {
    this.displayPartnerDetails();
  }
  displayPartnerDetails() {
    this.userprofileservice.GetPartnerProfileDataByUserID(this.userservice.getLoggedinUserId()).subscribe(data => {
      this.partnerprofiledetails = data.response[0];
    });
  }

}
