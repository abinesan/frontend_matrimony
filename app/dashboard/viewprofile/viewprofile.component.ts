import { Component, OnInit } from '@angular/core';
import { UserprofileService } from 'src/app/Service-Layer/userprofile.service';
import { UserService } from 'src/app/Service-Layer/user.service';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.less']
})
export class ViewprofileComponent implements OnInit {
  userprofiledetails: any;
  contactdetails: any;
  showMore: any;
  constructor(private userprofileservice: UserprofileService,
              private userservice: UserService) { }

  ngOnInit() {
    this.displayUserProfileDetails();
    this.displayContactDetails();
  }
  displayUserProfileDetails() {
    this.userprofileservice.GetUserProfileDataByUserID(this.userservice.getLoggedinUserId()).subscribe(data => {
      return this.userprofiledetails = data.response[0];
    });
  }
  displayContactDetails() {
    this.userprofileservice.GetContactDetailsByUserId(this.userservice.getLoggedinUserId()).subscribe(data => {
      return this.contactdetails = data.response;
    });
  }
}
