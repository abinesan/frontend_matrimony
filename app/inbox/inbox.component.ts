import { Component, OnInit } from '@angular/core';
import { InboxService } from '../Service-Layer/inbox.service';
import { UserService } from '../Service-Layer/user.service';
import { MessageService } from 'primeng/api';
import { MatchesService } from '../Service-Layer/matches.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.less']
})
export class InboxComponent implements OnInit {
  interestreceivedsearch_options: string[] = ['In 24 hrs', 'In 7 Days', 'All Received Interest'];
  interestsentsearch_options: string[] = ['In 7 Days', 'Sent Reminder', 'Cancelled Request'];

  accceptedsearch_options: string[] = ['Accepted by Me', 'Accepted by Them'];

  rejectedsearch_options: string[] = ['Rejected by Me', 'Rejected by Them'];




  all_interest_sent_profileList: any;
  all_interest_recived_profileList: any;
  all_interest_accpeted_profileList: any;
  all_interest_rejected_profileList: any;
  rejected_filter_option: string;
  accepted_filter_option: string;
  ismyprofilevisited = 0;
  constructor(private inboxservice: InboxService, private userservice: UserService,
    private messageService: MessageService, private matchesservice: MatchesService,
    private router:Router) { }

  ngOnInit() {
    this.DisplayAllInterestSentProfiles();
    this.DisplayAllInterestReceviedProfiles();
    this.DisplayAllAcceptedProfiles();
    this.DisplayAllRejectedProfiles();


  }

  DisplayAllInterestSentProfiles() {
    this.inboxservice.DisplayInterestSentList(this.userservice.getLoggedinUserId()).subscribe(data => {
      if (data.status == 'success') {
        this.all_interest_sent_profileList = data.userList;
      }
    });
  }

  DisplayAllInterestReceviedProfiles() {
    this.inboxservice.DisplayInterestReceivedList(this.userservice.getLoggedinUserId()).subscribe(data => {
      if (data.status == 'success') {
        this.all_interest_recived_profileList = data.userList;
      }
    });
  }
  DisplayAllAcceptedProfiles() {
    this.inboxservice.DisplayInterestAcceptedList(this.userservice.getLoggedinUserId()).subscribe(data => {
      if (data.status == 'success') {

        if (this.accepted_filter_option == 'accept_by_me') {          this.all_interest_accpeted_profileList = data.youAccepted;
        } else {           this.all_interest_accpeted_profileList = data.theyAccepted;
        }
      }

    });
  }
  DisplayAllRejectedProfiles() {    this.inboxservice.DisplayInterestRejectedList(this.userservice.getLoggedinUserId()).subscribe(data => {
      if (data.status == 'success') {

        if (this.rejected_filter_option == 'reject_by_me') {          this.all_interest_rejected_profileList = data.youRejected;
        } else {         this.all_interest_rejected_profileList = data.theyRejected;
        }

      }
    });
  }

  DisplayAcceptedFilterRecords(index) {
    if (this.accceptedsearch_options[index] == 'Accepted by Me') {
      this.accepted_filter_option = 'accept_by_me';
      this.DisplayAllAcceptedProfiles();
    } else if (this.accceptedsearch_options[index] == 'Accepted by Them') {
      this.accepted_filter_option = 'accept_by_them';
      this.DisplayAllAcceptedProfiles();
    }

  }

  DisplayRejectedFilterRecords(index) {
    if (this.rejectedsearch_options[index] == 'Rejected by Me') {
      this.rejected_filter_option = 'reject_by_me';
      this.DisplayAllRejectedProfiles();
    } else if (this.rejectedsearch_options[index] == 'Rejected by Them') {
      this.rejected_filter_option = 'reject_by_them';
      this.DisplayAllRejectedProfiles();
    }

  }

  SendAcceptRequestFromInterestReceived(selected_profile_id, user_interest) {    this.clearMessage();
       const formdata = {
      senderId: selected_profile_id,
      receiverId: this.userservice.getLoggedinUserId(),
      connectionStatus: user_interest
    };
       this.matchesservice.SendAccept(formdata).subscribe(data => {
      if (data.status == 'success') {        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
               this.DisplayAllInterestReceviedProfiles();
               this.DisplayAllAcceptedProfiles();

      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }
    });
  }

  SendAcceptRequestFromInterestSent(selected_profile_id, user_interest) {    this.clearMessage();
       const formdata = {
      senderId: this.userservice.getLoggedinUserId(),
      receiverId: selected_profile_id,
      connectionStatus: user_interest
    };
       this.matchesservice.SendAccept(formdata).subscribe(data => {
      if (data.status == 'success') {        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
        this.DisplayAllInterestSentProfiles();
        this.DisplayAllAcceptedProfiles();
        this.DisplayAllRejectedProfiles();
      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }
    });
  }

  viewmoredetails(visited_profileid, current_image_path, current_picture_rights) {
    debugger;
    // changing the value of is my profile visted value if we cliked on more button on UI
    this.ismyprofilevisited = 1;

    this.matchesservice.GetFullProfileViewData({
      senderId: parseInt(this.userservice.getLoggedinUserId()),
      receiverId: parseInt(visited_profileid),
      profile_view_status: this.ismyprofilevisited
    }).subscribe(data => {
      // setting the image
      this.matchesservice.SetCurrentiamge(current_image_path);
      this.matchesservice.SetCurrentImageRights(current_picture_rights);
      this.router.navigate(['/viewfullprofilewithId',visited_profileid]);
    });
  }


  clearMessage() {
    this.messageService.clear();
  }


}
