import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-join-now',
  templateUrl: './join-now.component.html',
  styleUrls: ['./join-now.component.less']
})
export class JoinNowComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
