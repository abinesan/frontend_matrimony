import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MatchesService } from 'src/app/Service-Layer/matches.service';

@Component({
  selector: 'app-viewfullimage',
  templateUrl: './viewfullimage.component.html',
  styleUrls: ['./viewfullimage.component.less']
})
export class ViewfullimageComponent implements OnInit {
  viewprofile_image_path: any;
  viewprofile_image_rights: any;

  constructor(public dialogRef: MatDialogRef<ViewfullimageComponent>,
              private matchservice: MatchesService, ) { }

  ngOnInit() {
    this.viewprofile_image_path = this.matchservice.GetCurrentImage();
    this.viewprofile_image_rights = this.matchservice.GetCurrentImageRights();
  }
  viewFullImage(temp_a, temp_b)
  {
    
  }

}
