import { Component, OnInit } from '@angular/core';
import { MatchesService } from 'src/app/Service-Layer/matches.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewfullimageComponent } from '../viewfullimage/viewfullimage.component';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/Service-Layer/user.service';



@Component({
  selector: 'app-viewfullprofile',
  templateUrl: './viewfullprofile.component.html',
  styleUrls: ['./viewfullprofile.component.less']
})
export class ViewfullprofileComponent implements OnInit {

  viewprofile_image_path: any;
  viewprofile_image_rights: any;
  PartnerPreference:any;
  ProfileData: any;
  BasicInfo: any;
  contactInfo: any;
  familyInfo: any;
  leisuresInfo: any;
  proffesionInfo: any;
  locationInfo: any;
  CardId:any;
  text = 'Tamil, English, Hindi, French, Kanada, Telugu, Marati';
  showMore = false;
  constructor(public dialog: MatDialog,
              private matchservice: MatchesService,private route: ActivatedRoute,private userservice: UserService ) { }

  ngOnInit() {
    debugger;
    var tell = parseInt(this.userservice.getLoggedinUserId())
    this.viewprofile_image_path = this.matchservice.GetCurrentImage();
    this.viewprofile_image_rights = this.matchservice.GetCurrentImageRights();
     this.CardId = this.route.snapshot.paramMap.get('CardId');
    this.matchservice.OpenMymatches({
      "cardUserId" : parseInt(this.CardId),
      "loginUserId"  : parseInt(this.userservice.getLoggedinUserId())
    }).subscribe((data:any)=> {
      debugger;
      if(data.status) {
        this.ProfileData =  data.response[0];
        this.BasicInfo = this.ProfileData.basic;
        this.contactInfo = this.ProfileData.contact;
        this.familyInfo = this.ProfileData.family;
        this.leisuresInfo = this.ProfileData.leisures;
        this.proffesionInfo = this.ProfileData.proffesion;
        this.locationInfo = this.ProfileData.location;
        this.matchPartner();
      }
    
    })
  }
  viewFullImage(): void {
    // this.matchservice.SetCurrentiamge(current_image_path);
    // this.matchservice.SetCurrentImageRights(current_picture_rights);
    const dialogRef = this.dialog.open(ViewfullimageComponent, {
      width: '550px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  matchPartner() {
debugger;

    this.matchservice.ViewpartnerMatchProfile({
      "cardUserId" : parseInt(this.CardId),
      "loginUserId"  : parseInt(this.userservice.getLoggedinUserId())
    }).subscribe(data => {
      debugger;
      this.PartnerPreference = data;
    })
  }
}
