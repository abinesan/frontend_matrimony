import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Service-Layer/user.service';
import { UserRegistration } from 'src/app/Core/Models/user-registration';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { OTPService } from 'src/app/Service-Layer/otp.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {


  txt_looking_for = '';
  txt_user_email = '';
  txt_user_password = '';
  ddl_profile_select = '';

  is_otp_succes = false;

  is_enable_mobileno = false;
  listofprofiles = ['Self', 'Son', 'Daughter', 'Brother', 'Sister', 'Friend', 'Relative'];
  constructor(private userservice: UserService,
              private router: Router,
              private messageService: MessageService,
              private otpservice: OTPService,
              private datePipe: DatePipe) { }



  ngOnInit() {
  }
  changeMobile() {
    this.is_enable_mobileno = true;
  }
  resendOTP(country_code, user_mobile) {    const valid_mobileno = '+' + country_code + user_mobile;
       this.otpservice.GenarateOTP(valid_mobileno).subscribe(data => {
      if (data.Status == 'Success') {        const sessionid = data.Details;
                                             this.otpservice.SetCurrentValidSessionID(sessionid);
                                             this.messageService.add({ severity: 'success', summary: 'OTP has been re-sent to your given Mobile Number', detail: ' ' });
       // this.redirectToPage();

      } else {
        this.messageService.add({ severity: 'warn', summary: data.message , detail: ' ' });
      }
    });
  }
  verifyOTP(user_OTP) {    this.clearMessage();
       const valid_sessionid = this.otpservice.GetCurrentValidSessionID();
       this.otpservice.VerifyOTP(valid_sessionid, user_OTP).subscribe(data => {
      if (data.Status == 'Success') {
        this.is_otp_succes = true;
        this.messageService.add({ severity: 'success', summary: data.Details, detail: ' ' });
        return;
      } else if (data.Status == 'Error') {
        this.is_otp_succes = false;
        this.is_enable_mobileno = true;
        this.messageService.add({ severity: 'success', summary: data.Details, detail: ' '});
      } else {

        this.messageService.add({ severity: 'success', summary: 'Something went wrong while Verifing OTP', detail: ' '});
      }
    });
  }




  registerUser(formdata) {
    debugger;
    this.clearMessage();
    const current_page_data = formdata;
    const previous_four_formsdata = this.userservice.getUserRegistrationPageDetails();

    let reg_user_gender = '';
    const alluserDetails = {...current_page_data, ...previous_four_formsdata};

    if (alluserDetails.create_profile_for == 'Son' || alluserDetails.create_profile_for == 'Brother') {
      alluserDetails.I_am_looking_for = 'Female';
      reg_user_gender = 'Male';
    } else if (alluserDetails.create_profile_for == 'Daughter' || alluserDetails.create_profile_for == 'Sister') {
      alluserDetails.I_am_looking_for = 'Male';
      reg_user_gender = 'Female';
    }
    else
    {
      reg_user_gender = alluserDetails.I_am_looking_for;
      if (alluserDetails.I_am_looking_for == "Female")
      {
        alluserDetails.I_am_looking_for = 'Male';
        reg_user_gender = 'Female';
      }
      else
      {
        alluserDetails.I_am_looking_for = 'Female';
        reg_user_gender = 'Male';
      }
    }

    debugger;
    let mydate = alluserDetails.DOBYear + '-' + alluserDetails.DOBMonth + '-' + alluserDetails.DOBDay;
   
    let myformat_date = new Date(mydate);
    let user_dob = this.datePipe.transform(myformat_date, 'yyyy-MM-dd');
    const user_income = parseInt(alluserDetails.ddl_user_income.split('-')[0]);
    const user_height = parseInt(alluserDetails.ddl_user_height);
    let full_mobilenumber = alluserDetails.mobileCountryCode + alluserDetails.mobileNumber;
    const finalformdata = {
      // getting data from regiser page
      email: alluserDetails.txt_user_email,
      password: alluserDetails.txt_user_password,

      // extra fields
      gender: reg_user_gender,
      // getting data from home page
      fullName: alluserDetails.fullName,
      mobileCountryCode: alluserDetails.mobileCountryCode,
      mobileNumber: full_mobilenumber, // sending mobile with code
      create_profile_for: alluserDetails.create_profile_for,
      i_am_looking_for: alluserDetails.I_am_looking_for,

      // getting data from first form

      dateOfBirth: user_dob,
      height: user_height,
      physicallyChallenged: alluserDetails.ddl_user_physicalchallange,
      motherTongue: alluserDetails.ddl_user_mothertongue,
      caste: alluserDetails.ddl_user_caste,
      religion:alluserDetails.ddl_user_religion,
      subCaste: alluserDetails.txt_user_subcaste,
      gothram: alluserDetails.ddl_user_gothram,
      dosham: alluserDetails.ddl_user_dosham,

     // "maritalStatus":"Never Married",
     // "weight":55,

     // getting the data from second form
      familyType: alluserDetails.ddl_user_familytype,
      residenceCountry: alluserDetails.ddl_user_country,
      residenceState: alluserDetails.ddl_user_state,
      residenceCity: alluserDetails.ddl_user_city,
      bridegroomCitizenship: alluserDetails.txt_user_citizan,
      bgVisaStatus: alluserDetails.txt_user_visastatus,

      // getting the data from third form
      education: alluserDetails.ddl_user_education,
      employement: alluserDetails.ddl_user_employment,
      occupation: alluserDetails.ddl_user_occupation,
      annualIncome: user_income,

    };
    if (this.is_otp_succes == true) {        this.userservice.AddRegistration(finalformdata).subscribe(response => {
          if (response.status == 'success') {
            this.messageService.add({ severity: 'success', summary: response.message, detail: ' ' });
            this.redirectToPage();

          } else {
            debugger;
            this.messageService.add({ severity: 'warn', summary: response.message, detail: ' ' });

          }
        },
          err => {
            this.messageService.add({ severity: 'error', summary: 'Sommething went wrong while registration', detail: err });
            console.log(err);

          });
    } else {
      this.messageService.add({ severity: 'warn', summary: 'OTP Invalid ', detail: ' ' });
    }

  }

  clearMessage() {
    this.messageService.clear();
  }
  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/signup']);
    }, 3000);
  }

}
