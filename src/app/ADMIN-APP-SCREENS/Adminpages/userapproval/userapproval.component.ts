import { Component, OnInit } from '@angular/core';
import { AdminuserService } from '../../Admin-Service-Layer/adminuser.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-userapproval',
  templateUrl: './userapproval.component.html',
  styleUrls: ['./userapproval.component.less']
})
export class UserapprovalComponent implements OnInit {
  all_user_list: any;
  searchText: any;
  radio_accept_status = false;
  radio_reject_status = false;

  chk_approval_status: any;
  ddl_status = '';

  status_list = ['accept', 'pending', 'reject', 'delete'];
  selected_filter = '';
  temp_all_user_list: any[];
  male_button_color: any;
  female_button_color: any;
  is_male_button_clicked = true;
  is_female_button_clicked = true;

  constructor(private router: Router, private adminuserservice: AdminuserService,
              private messageService: MessageService, ) { }

  ngOnInit() {
    // on page load display all pending users
    this.DisplayAllUserListByStatus('pending');
    this.temp_all_user_list = this.all_user_list;


  }
  selectedstatus(selected_status) {    if (selected_status == 'reject') {
      this.radio_reject_status = true;
    } else {
      this.radio_reject_status = false;
    }

                                       if (selected_status == 'accept') {
      this.radio_accept_status = true;
    } else {
      this.radio_accept_status = false;
    }

                                       this.selected_filter = selected_status;
                                       this.DisplayAllUserListByStatus(selected_status);
  }
  filterMaleUserList(gender) {
    if (this.is_male_button_clicked == true) {
      this.male_button_color = 'primary';
      this.female_button_color = 'secondary';
    }
    this.temp_all_user_list = this.DisplayAllUserListByStatus(this.selected_filter);
    this.all_user_list = this.temp_all_user_list.filter(x => x.gender == gender);
  }
  filterFemaleUserList(gender) {
    if (this.is_female_button_clicked == true) {
      this.female_button_color = 'primary';
      this.male_button_color = 'secondary';

    }    this.all_user_list = this.all_user_list.filter(x => x.gender == gender);

  }

  searchusers() {    return this.all_user_list.filter(item => {
      return Object.keys(item).some(key => {
        return item[key].toLowerCase().includes(this.searchText.toLowerCase());
      });
    });
  }
  updatestatus(selected_option) {
    alert(selected_option + '--' );
    console.log(selected_option);
  }

  DisplayAllUserListByStatus(status): any {
    this.adminuserservice.GetUserListByStatus(status).subscribe(data => {
      this.all_user_list = data.response;
    });
  }



  SaveUserApprovalStatus(selected_used_id, selected_approval_status) {
    this.clearMessage();
    const formdata = {
      approval_status: selected_approval_status,
      userId: selected_used_id
    };

    this.adminuserservice.UserApprovalStatus(formdata).subscribe(data => {
      this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
      this.DisplayAllUserListByStatus(this.selected_filter);
    });
  }

  clearMessage() {
    this.messageService.clear();
  }

}
