import { Injectable } from '@angular/core';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) { }

  private chat = new BehaviorSubject<any>('');
  castchat = this.chat.asObservable();

  sendChat(chatid) {
    // this.chat.next(newChat);
    this.chat.next(this.getMessagesListByChatID(chatid));
  }
  getChatRoomID(userinfo): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_CHATROOM_ID;
    return this.http.post(url, userinfo);
  }

  getMessagesListByChatID(chatinfo): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_MESSAGE_LIST_INFO_CHAT_ID;
    return this.http.post(url, chatinfo);
  }

  SendNewMessage(newmsg_info): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_INSERT_NEW_MESSAGE;
    return this.http.post(url, newmsg_info);
  }
  GetNewChatUserList(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_NEW_CHATUSERS_LIST_ID + '/' + current_userid;
    return this.http.get(url);
  }
  GetOldChatUserList(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_OLD_CHATUSERS_LIST_ID + '/' + current_userid;
    return this.http.get(url);
  }
}
