import { Pipe, PipeTransform } from '@angular/core';
import { parse } from 'querystring';

@Pipe({
  name: 'amountdesc'
})
export class AmountdescPipe implements PipeTransform {
  FinalAmount: any;
  transform(value: any, ...args: any[]): any {    if (value >= 10000000) {
      this.FinalAmount = value + ' C ';
    } else {
      this.FinalAmount = value + ' L ';
    }
                                                  return this.FinalAmount;
  }

}
