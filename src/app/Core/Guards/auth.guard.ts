import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/Service-Layer/user.service';
import { AdminUserService } from 'src/app/Service-Layer/Admin-Service-Layer/admin-user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private userservice: UserService, private adminservice: AdminUserService) {


  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (localStorage.getItem('role') == 'admin') {      return true;
    }

    if (this.userservice.getAuthenticationTokens() != null) {
      return true;
    } else {
      return false;
    }

    // if(this.adminservice.AdminUserLogin())

  }

}
