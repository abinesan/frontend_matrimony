import { environment } from 'src/environments/environment';

export class ApplicationUrls {
    public static BASE_URL = 'http://91.92.128.8:8000/matrimony/API'; // 'http://82.165.120.236:8000/matrimony/API';

    // OTP Service Url
    // public static BASE_OTP_URL = 'https://2factor.in/API/V1/' + environment.otp_api_key + '/SMS/';
    public static BASE_OTP_URL = 'http://91.92.128.8:8000/matrimony/API/otp/';
    // User Rest Services Urls
    public static USER_LOGIN_URL = '/user/login';
    public static USER_REGISTRATION_URL = '/user/register';

    public static USER_LOGOUT = '/user/logout';

    public static USER_PROFILE_URL = '/myprofile/add/update';

    public static GET_USER_PROFILE_DATA_BY_USERID = '/myProfileList';

    public static PARTNER_PROFILE_URL = '/partner/preference/add/update';

    public static GET_PARTNER_PROFILE_DATA_BY_USERID = '/partner/preference/List';

    public static POST_CONTACT_ADD_UPDATE_URL = '/contact/detail/add/update';

    public static GET_CONTACT_DETAILS_BY_USERID = '/my/contact/details/List';

    public static GET_PARTNER_MATCHES_BY_ID = '/partner/profile/search';

    public static GET_NEW_CHATUSERS_LIST_ID = '/chat/new/user/list';

    public static GET_OLD_CHATUSERS_LIST_ID = '/chat/old/user/list';

    public static POST_PARTNER_VIEW_PROFILE_STATUS_BY_USERID = '/profile/visitor/add';

    public static POST_CHATROOM_ID = '/chat/provide';

    public static POST_MESSAGE_LIST_INFO_CHAT_ID = '/chat/list';

    public static POST_INSERT_NEW_MESSAGE = '/chat/add';

    public static GET_ALLCOUNTRIES_LIST = '/country/list';

    public static GET_STATELIST_BY_COUNTRY_ID = '/state/list';

    public static GET_CITYLIST_BY_STATE_ID = '/city/list';

    public static GET_CASTE_LIST = '/dropdown/caste/list';

    public static GET_MOTHER_TONGUE_LIST = '/dropdown/mothertongue/list';

    public static GET_EDUCATION_LIST = '/dropdown/education/list';


    public static GET_NAKSHATRA_LIST = '/dropdown/nakshatra/list';

    public static GET_RASI_LIST = '/dropdown/rasi/list';

    public static GET_GOTHRAM_LIST = '/dropdown/gothram/list';
    public static GET_PARTNER_OCCUPATION_LIST = '/dropdown/occupation/list';
    public static GET_PARTNER_EDUCATION_LIST = '/dropdown/education/list';

    public static POST_PARTNER_STATELIST_BY_COUNTRY_IDS = '/partner/state/list';

    public static POST_PARTNER_CITYLIST_BY_STATE_IDS = '/partner/city/list';

    public static GET_RECENTLY_VIEWED_PROFILES_LIST_BY_ME = '/viewed/profile/by/me';
    public static GET_RECENTLY_VIEWED_PROFILES_LIST_BY_OTHERS = '/my/profile/viewed/by/others';
    public static GET_NEAR_ME_MATCHES_LIST = '/matches/nearme/search';
    public static GET_ALL_MATCHES_LIST = '/matches/view/all/profile';

    public static GET_NEW_MATCHES_FILTER_LIST = '/filter/mymatches/list';

    public static POST_CONTACT_DETAILS = '/contact/detail/add/update';
    public static POST_PROFILE_ACCPET = '/profile/profile/accept/request/add';
    public static GET_INTEREST_SENT_LIST = '/profile/intrest/sent';
    public static GET_INTEREST_RECEIVED_LIST = '/profile/intrest/received';
    public static GET_INTEREST_ACCEPT_LIST = '/profile/intreqst/accept/list';
    public static GET_INTEREST_REJECTED_LIST = '/profile/rejected/status/list';

    public static POST_CHANGE_PASSWORD = '/change/password';
    public static POST_FORGOT_PASSWORD = '/forgot/password/token';
    public static POST_VALIDATE_PASSWORD = '/password/token/validation';
    public static POST_RESET_PASSWORD = '/forgot/password/reset';

    //
    // *************admin api url list start here ********************
    public static ADMIN_USER_LOGIN_URL = '/admin/login';
    public static GET_USERS_LIST_BY_STATUS = '/user/list';
    public static POST_USER_APPROVAL_STATUS = '/admin/approval/status';

    // *************admin api url list ends here ********************

     // *************Abinesh Changes*****************************
     public static  VIEW_FULL_CARD_PROFILE = '/view/profile/in/matches'

     public static VIEW_MATCHES_PARTNER_PREFERENCE = '/view/partner/matches/profile'

}
