import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ApplicationUrls } from 'src/app/Core/Models/application-urls';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AdminUserService {

  constructor(private http: HttpClient) { }

  AdminUserLogin(userForm): Observable<any> {    const url = ApplicationUrls.BASE_URL + ApplicationUrls.ADMIN_USER_LOGIN_URL;
                                                 return this.http.post(url, userForm).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {    console.log(error);
                                                    return throwError(error.message);
  }
}
