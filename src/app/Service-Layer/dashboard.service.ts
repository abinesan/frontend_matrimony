import { Injectable } from '@angular/core';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http:HttpClient) { }

  getdashboardList(loggedin_userid):Observable<any>
  {
    let url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_DASHBOARD_LIST_BY_USERID + '/' + loggedin_userid;
    return this.http.get(url);
  }
}
