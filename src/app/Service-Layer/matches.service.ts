import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MatchesService {

  selected_image_path: any;
  selected_image_rights: any;
  constructor(private http: HttpClient ) { }

  GetTodayMatchesProfileData(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_PARTNER_MATCHES_BY_ID + '/' + current_userid;
    return this.http.get(url);
  }
  GetRecentlyViewedProfileListByMe(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_RECENTLY_VIEWED_PROFILES_LIST_BY_ME + '/' + current_userid;
    return this.http.get(url);
  }
  GetRecentlyViewedProfileListByOthers(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_RECENTLY_VIEWED_PROFILES_LIST_BY_OTHERS + '/' + current_userid;
    return this.http.get(url);
  }
  GetNearMeMatchedProfileList(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_NEAR_ME_MATCHES_LIST + '/' + current_userid;
    return this.http.get(url);
  }
  GetAllMatchesProfileList(current_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_ALL_MATCHES_LIST + '/' + current_userid;
    return this.http.get(url);
  }


  GetFullProfileViewData(viewprofiledata_by_userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_PARTNER_VIEW_PROFILE_STATUS_BY_USERID;
    return this.http.post(url, viewprofiledata_by_userid);
  }

  GetNewMatchesFilterList(): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.GET_NEW_MATCHES_FILTER_LIST;
    return this.http.get(url);
  }

  SendAccept(profile_accept_info): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_PROFILE_ACCPET;
    return this.http.post(url, profile_accept_info);
  }




  GetCurrentImage() {
    return this.selected_image_path;
  }
  SetCurrentiamge(imagepath) {
    this.selected_image_path = imagepath;
  }
  GetCurrentImageRights() {
    return this.selected_image_rights;
  }
  SetCurrentImageRights(imagerights) {
    this.selected_image_rights = imagerights;
  }

   //************Abinesh Changes***************

   OpenMymatches(UserIds) {
    let url = ApplicationUrls.BASE_URL+ApplicationUrls.VIEW_FULL_CARD_PROFILE
    return this.http.post(url,UserIds);
  }

  ViewpartnerMatchProfile(UserIds) {
    let url = ApplicationUrls.BASE_URL+ApplicationUrls.VIEW_MATCHES_PARTNER_PREFERENCE
    return this.http.post(url,UserIds);
  }


}
