import { Injectable } from '@angular/core';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OTPService {

  Current_valid_session_id: any;
  constructor(private http: HttpClient) { }

  GenarateOTP(user_mobile_no): Observable<any> {
    // let url = ApplicationUrls.BASE_OTP_URL + user_mobile_no+'/AUTOGEN';
    const url =  ApplicationUrls.BASE_OTP_URL + 'send/' + user_mobile_no;
    return this.http.get(url);
  }
  VerifyOTP(valid_session_id, otp_num): Observable<any> {
    // let url = ApplicationUrls.BASE_OTP_URL + 'VERIFY/' + valid_session_id + '/' + otp_num;
    const url = ApplicationUrls.BASE_OTP_URL + 'verify/' + valid_session_id + '/' + otp_num;
    return this.http.get(url);
  }
  GetCurrentValidSessionID() {
    return localStorage.getItem('otpsessionid');
  // return this.Current_valid_session_id;
  }
  SetCurrentValidSessionID(session_id) {
    localStorage.setItem('otpsessionid', session_id);
   // this.Current_valid_session_id = session_id;
  }
}
