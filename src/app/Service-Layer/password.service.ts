import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  verification_email_userid: number;
  constructor(private http: HttpClient) { }


  ChangePassword(formdata): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_CHANGE_PASSWORD;
    return this.http.post(url, formdata);
  }

  ForgotPassword(formdata): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_FORGOT_PASSWORD;
    return this.http.post(url, formdata);
  }

  ValidatePassword(formdata): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_VALIDATE_PASSWORD;
    return this.http.post(url, formdata);
  }

  ResetPassword(formdata): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.POST_RESET_PASSWORD;
    return this.http.post(url, formdata);
  }

  GetVerificationEmail_UserId() {
    return this.verification_email_userid;
  }
  SetVerificationEmail_UserId(verification_userid) {
    this.verification_email_userid = verification_userid;
  }

}
