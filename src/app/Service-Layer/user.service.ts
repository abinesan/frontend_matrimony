import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ApplicationUrls } from '../Core/Models/application-urls';
import { UserRegistration } from '../Core/Models/user-registration';
import { UserLogin } from '../Core/Models/user-login';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  Loggedinuserid: any;
  Api_access_token: any;
  Api_refresh_token: any;
  Api_Loggedin_userId: any;
  finalTooken: any;

  Current_user_basic_details = {};
  Current_user_registration_details = {};
  display_permission = '';

  Current_user_mobilenumber: any;
  constructor(private http: HttpClient, private router: Router) { }

  IsAdminUser() {
    if (localStorage.getItem('role') == 'admin') {
      return true;
    } else if (localStorage.getItem('role') == 'user') {
      return false;
    } else {
      return false;
    }
  }

  getUserRole() {

    return localStorage.getItem('role');
  }
  setUserRole(rolename) {
    localStorage.setItem('role', rolename);

  }
  addUser(userForm): Observable<any> {
    return this.http.post(ApplicationUrls.BASE_URL + ApplicationUrls.USER_REGISTRATION_URL, userForm).pipe(
      catchError(this.handleError)
    );
  }

  AddRegistration(userform): Observable<any> {
    return this.http.post(ApplicationUrls.BASE_URL + ApplicationUrls.USER_REGISTRATION_URL, userform).pipe(
      catchError(this.handleError)
    );
  }
  getUserbasicDetails() {
    return this.Current_user_basic_details;
  }
  setUserBasicDetails(userbasicdetails) {
    this.Current_user_basic_details = userbasicdetails;
  }
  getUserMobileNumber() {
    return this.Current_user_mobilenumber;
  }
  setUserMobileNumber(mobileno) {
    this.Current_user_mobilenumber = mobileno;
  }
  getUserRegistrationPageDetails() {
    return this.Current_user_registration_details;
  }
  setUserRegistrationPageDetails(user_registration_details) {
    this.Current_user_registration_details = user_registration_details;
  }

  userLogin(userForm): Observable<any> {
    return this.http.post(ApplicationUrls.BASE_URL + ApplicationUrls.USER_LOGIN_URL, userForm).pipe(
      catchError(this.handleError)
    );
  }
  getLoggedinUserId() {
    return localStorage.getItem('userId');
  }
  setLoggedinUserId(currentuserid) {
    localStorage.setItem('userId', currentuserid);
  }

  getAuthenticationTokens() {
    // return this.finalTooken;
    this.finalTooken = localStorage.getItem('finalToken');
    return localStorage.getItem('finalToken');

  }
  setAuthenticationTokens(server_acess_token, server_refresh_token, server_Loggedin_userid) {
    // this.Api_access_token = server_refresh_token;
    // this.Api_refresh_token = server_refresh_token;
    // this.Api_Loggedin_userId = server_Loggedin_userid;
    // this.finalTooken = "access_token: " + this.Api_access_token + " " + "refresh_token: " + this.Api_refresh_token + " " + "userid: " + this.Api_Loggedin_userId;

    // not working when we use localstorage
    localStorage.setItem('finalToken', 'access_token: ' + server_acess_token + ' ' + 'refresh_token: ' + server_refresh_token + ' ' + 'userId: ' + server_Loggedin_userid);
  }
  CurrentUserLogout(userid): Observable<any> {
    const url = ApplicationUrls.BASE_URL + ApplicationUrls.USER_LOGOUT;
    return this.http.post(url, userid);
  }
  userLogout() {    if (this.router.url.includes('admin')) {      this.router.navigate(['/admin']);
    } else {      this.CurrentUserLogout(localStorage.getItem('userId')).subscribe(data => {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('refreshToken');
        localStorage.removeItem('userId');
        localStorage.removeItem('finalToken');
        localStorage.removeItem('role');
        this.router.navigate(['/signup']);

      });
    }
  }

  private handleError(error: HttpErrorResponse) {    console.log(error);
                                                     return throwError(error.message);
  }

}
