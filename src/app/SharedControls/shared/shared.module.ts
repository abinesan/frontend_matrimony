import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import {PasswordModule} from 'primeng/password';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { MultiSelectModule } from 'primeng/multiselect';
import { CheckboxModule } from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,

    MessagesModule,
    ToastModule,
    PasswordModule,
    AutoCompleteModule
  ],
  exports: [
    CommonModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    PasswordModule,
    AutoCompleteModule,
    MultiSelectModule,
    CheckboxModule,
    RadioButtonModule


   ],
  providers: [MessageService]
})
export class SharedModule { }
