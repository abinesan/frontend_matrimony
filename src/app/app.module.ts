import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { BnNgIdleService } from 'bn-ng-idle'; // import bn-ng-idle service
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {DatePipe} from '@angular/common';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { WhychooseComponent } from './whychoose/whychoose.component';
import { CarouselComponent } from './carousel/carousel.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { GestureConfig, MatBadgeModule } from '@angular/material';
import { JoinNowComponent } from './join-now/join-now.component';
import { HomeComponent } from './home/home.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { RegisterComponent } from './signup/register/register.component';
import { MatchesComponent } from './matches/matches.component';
import { PricingComponent } from './pricing/pricing.component';
import { SearchComponent } from './search/search.component';
import { InboxComponent } from './inbox/inbox.component';
import { EditProfileComponent } from './dashboard/edit-profile/edit-profile.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UserService } from './Service-Layer/user.service';
import { ViewfullprofileComponent } from './matches/viewfullprofile/viewfullprofile.component';
import { SharedModule } from './SharedControls/shared/shared.module';

// prime ng library for Toast notifications
import {ToastModule} from 'primeng/toast';
import { PartnerPreferenceComponent } from './dashboard/partner-preference/partner-preference.component';
import { ViewprofileComponent } from './dashboard/viewprofile/viewprofile.component';
import { DummyMatchesComponent } from './dummy-matches/dummy-matches.component';
import { TokenInteceptorService } from './Core/AuthService/token-inteceptor.service';
import { AmountdescPipe } from './Core/CustomPipes/amountdesc.pipe';
import { AuthGuard } from './Core/Guards/auth.guard';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { MatchesService } from './Service-Layer/matches.service';
import { ChatModule } from './CHATAPP/chat/chat.module';
import { AdminGuard } from './Core/Guards/admin.guard';
import { LoginComponent } from './ADMIN-APP-SCREENS/Adminpages/login/login.component';

import {Ng2TelInputModule} from 'ng2-tel-input';
import { RegistrationFormComponent } from './signup/registration-form/registration-form.component';
import { AdminDashboardComponent } from './ADMIN-APP-SCREENS/Adminpages/admin-dashboard/admin-dashboard.component';
import { UserapprovalComponent } from './ADMIN-APP-SCREENS/Adminpages/userapproval/userapproval.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { NewPasswordComponent } from './forgot-password/new-password/new-password.component';
import { InboxService } from './Service-Layer/inbox.service';
import { AdminuserService } from './ADMIN-APP-SCREENS/Admin-Service-Layer/adminuser.service';
import { FilterPipe } from './Core/CustomPipes/filter.pipe';
import { ManageadsComponent } from './ADMIN-APP-SCREENS/Adminpages/manageads/manageads.component';
import { PasswordService } from './Service-Layer/password.service';
import { ViewfullimageComponent } from './matches/viewfullimage/viewfullimage.component';
import { ImageuploadComponent } from './dashboard/edit-profile/imageupload/imageupload.component';
import { AdminUserService } from './Service-Layer/Admin-Service-Layer/admin-user.service';



export class CustomHammerConfig extends HammerGestureConfig  {
  overrides = {
      pinch: { enable: false },
      rotate: { enable: false }
  };
}
@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    HeaderComponent,
    BannerComponent,
    AboutComponent,
    FooterComponent,
    WhychooseComponent,
    CarouselComponent,
    JoinNowComponent,
    HomeComponent,
    MainNavComponent,
    DashboardComponent,
    SignupComponent,
    RegisterComponent,
    MatchesComponent,
    PricingComponent,
    SearchComponent,
    InboxComponent,
    EditProfileComponent,
    ForgotPasswordComponent,
    ViewfullprofileComponent,
    PartnerPreferenceComponent,
    ViewprofileComponent,
    DummyMatchesComponent,
    AmountdescPipe,
    TermsAndConditionsComponent,
    PrivacyPolicyComponent,
    LoginComponent,
    RegistrationFormComponent,
    AdminDashboardComponent,
    UserapprovalComponent,
    ResetPasswordComponent,
    NewPasswordComponent,
    ManageadsComponent,
    ViewfullimageComponent,
    ImageuploadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgxCarouselModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatBadgeModule,
    SharedModule,
    ToastModule,
    NgbModule,
    ChatModule,
    Ng2TelInputModule
  ],
  entryComponents: [
    ViewfullimageComponent,
    ImageuploadComponent
  ],

  providers: [UserService, AuthGuard, MatchesService, AdminGuard, DatePipe, InboxService, AdminUserService, PasswordService,
    {provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig},
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
   {provide: HTTP_INTERCEPTORS, useClass: TokenInteceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
