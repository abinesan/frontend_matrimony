import { Component, OnInit } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.less']
})
export class CarouselComponent implements OnInit {

  public carouselDiscountedCards: NgxCarousel;
  public carouselDiscountedCardItems: Array<any>;
  jobCardcolors: string[] = [
    '#5D4037', '#AB47BC', '#512DA8', '#EF6C00', '#00897B', '#689F38', '#455A64', '#EC407A', '#33691E', '#AB47BC'
  ];

  constructor(public router: Router) { }

  ngOnInit() {
    this.carouselDiscountedCardItems = [
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c2.jfif', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'},
      {img: 'assets/images/c1.jpg', name: 'Animesh & Saumya', duration: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'}
    ];
    this.carouselDiscountedCards = {
      grid: {xs: 1, sm: 1, md: 1, lg: 5, all: 0},
      slide: 1,
      speed: 400,
      interval: 4000,
      point: {
        visible: false,
        pointStyles: `
          .ngxcarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 12px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            position: absolute;
            width: 100%;
            bottom: 20px;
            left: 0;
            box-sizing: border-box;
          }
          .ngxcarouselPoint li {
            display: inline-block;
            border-radius: 999px;
            background: rgba(255, 255, 255, 0.55);
            padding: 5px;
            margin: 0 3px;
            transition: .4s ease all;
          }
          .ngxcarouselPoint li.active {
              background: white;
              width: 10px;
          }
        `
      },
      load: 2,
      loop: true,
      custom: 'banner',
      touch: true,
      easing: 'ease',
      animation: 'lazy'
    };

  }

}
