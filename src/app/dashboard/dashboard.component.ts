import { Component, OnInit, ViewChild } from '@angular/core';
import { TimeoutService } from '../Service-Layer/timeout.service';
import { BnNgIdleService } from 'bn-ng-idle'; // import it to your component
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['ProjectName', 'Customer', 'Status', 'MilestoneData', 'ProjectType', 'ProImg', 'ProjectManagar', 'SalesContact', 'HoursSpent', 'ActiveResources'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  ProjectName: string;
  Customer: string;
  Status: string;
  MilestoneData: string;
  ProjectType: string;
  ProjectManagar: string;
  SalesContact: string;
  HoursSpent: string;
  ActiveResources: string;
  ProImg: string;

}

const ELEMENT_DATA: PeriodicElement[] = [

  {ProjectName: 'Project A', Customer: 'TJX', Status: 'Planning',  MilestoneData: 'Sprint 2', ProjectType: 'Commercial', ProjectManagar: 'Philip', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '200 Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project B', Customer: 'DB', Status: 'Under Dev', MilestoneData: 'Sprint 6', ProjectType: 'R&D', ProjectManagar: 'Scott', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '17k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project C', Customer: 'e-Boks', Status: 'Testing', MilestoneData: 'Sprint 3', ProjectType: 'Commercial', ProjectManagar: 'Nick', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '33k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project D', Customer: 'CHUMS', Status: 'Inproduction',  MilestoneData: 'Sprint 6', ProjectType: 'Product Dev', ProjectManagar: 'Christi', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '30k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project E', Customer: 'China Lake', Status: 'Planning',  MilestoneData: 'Sprint 2', ProjectType: 'Internal', ProjectManagar: 'Luo', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '140 Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project F', Customer: 'NGC', Status: 'Testing',  MilestoneData: 'Sprint 1', ProjectType: 'Commercial', ProjectManagar: 'Philip', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '40k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project G', Customer: 'VA MUMPS', Status: 'Under Dev',  MilestoneData: 'Sprint 4', ProjectType: 'R&D', ProjectManagar: 'Scott', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '28k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project H', Customer: 'TJX', Status: 'Inproduction',  MilestoneData: 'Sprint 3', ProjectType: 'Commercial', ProjectManagar: 'Nick', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '42k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project I', Customer: 'DB', Status: 'Inproduction',  MilestoneData: 'Sprint 1', ProjectType: 'Internal', ProjectManagar: 'Christi', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '38k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project J', Customer: 'e-Boks', Status: 'Testing',  MilestoneData: 'Sprint 3', ProjectType: 'R&D', ProjectManagar: 'Luo', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '29k Hrs', ActiveResources: 'Hydrogen'},

  {ProjectName: 'Project K', Customer: 'NGC', Status: 'Testing',  MilestoneData: 'Sprint 5', ProjectType: 'Internal', ProjectManagar: 'Nick', ProImg: 'assets/images/interest.png', SalesContact: 'Philip', HoursSpent: '20k Hrs', ActiveResources: 'Hydrogen'},

];
