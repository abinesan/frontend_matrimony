import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { UserprofileService } from 'src/app/Service-Layer/userprofile.service';
import { UserService } from 'src/app/Service-Layer/user.service';

import { Message } from 'primeng//api';
import { MessageService } from 'primeng/api';
import { FormControl } from '@angular/forms';
import { ImageuploadComponent } from './imageupload/imageupload.component';
import { debugOutputAstAsTypeScript } from '@angular/compiler';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.less']
})
export class EditProfileComponent implements OnInit {
  // -----------contact profile end----------

  constructor(private userprofile: UserprofileService,
              public dialog: MatDialog,
              private userservice: UserService,
              private messageService: MessageService) { }
  is_residency_country_edit = false;
  is_residency_state_edit = false;
  is_residency_city_edit = false;

  is_groom_country_edit = false;
  is_groom_state_edit = false;
  is_groom_city_edit = false;
  favoriteSeason: any;
  is_partner_residency_country_edit = false;


  public contactdeatils: any[] = [{
    txt_cnt_mobile_no: '',
    txt_cnt_email: ''
  }];
  public contactdeatilsgroom: any[] = [{
    txt_cntgr_mobile_no: '',
    txt_cntgr_email: ''
  }];
  partnerprofiledetails: any;
  userprofiledetails: any;
  // contactdetails= {
  //   mobile:'',
  //   contact_person: '',
  //   contact_email: '',
  //   conventient_time_to_call: '',
  //   relationship_with_the_member: '',
  //   display_option: ''
  // };
  contactdetails: any;
  all_country_list = [];
  all_state_list = [];
  all_city_list = [];
  all_groom_country_list = [];
  all_groom_state_list = [];
  all_groom_city_list = [];

  ddl_country_id: number;
  ddl_state_id: number;
  ddl_city_id: number;

  ddl_resendency_country_id: number;
  ddl_resendency_state_id: number;
  ddl_resendency_city_id: number;

  DefaultData: any;
  permissions: string[] = ['Visible to all members', 'Visible to premium members only'];

  // **********start partner profile fields ****************
  marital_status_list = [
    'Any', 'Never Married', 'Widow', 'Divorced'
  ];
  cbl_marital_status: any;
  Apperance_list = [
    'Fair', 'Whitish', 'Dark', 'Doesn\'t Matter'
  ];
  rbl_apperance_status = '';
  Eating_habits_list = [
    'Veg', 'Non-veg', 'Eggitarian', 'Vegan', 'Pescatarian'
  ];
  cbl_eating_status: any;
  Smoke_habits_list = [
   'Never', 'Social', 'Regular'
  ];
  rbl_smoke_status = '';
  Drink_habits_list = [
    'Never', 'Occasional', 'Regular'
  ];
  rbl_drink_status = '';
  ddl_partner_religion: any;
  ddl_partner_lang_known: any;
  ddl_partner_caste: any;
  rbl_dosham_status = '';
  ddl_partner_res_country: any;
  ddl_partner_res_state: any;
  ddl_partner_res_city: any;
  relocation_list = [
    'Possible', 'Not Possible', 'Any'
  ];
  rbl_relocation_status = '';
  ddl_partner_employment: any;
  ddl_partner_occupation: any;
  ddl_partner_education: any;
  ddl_partner_income: any;
  ddl_partner_income_from: any;
  ddl_partner_income_to: any;
  all_parnter_country_list: any[];
  all_partner_state_list: any[];
  all_partner_city_list: any;
  parter_education_list: any;
  selected_occupation_id: any;
  partner_occupation_list: any;
  partner_employment_list = [
    'Government / Public Sector',
    'Defense / Civil Services',
    'Businees / Self Employed',
    'Not Working'
  ];
  partner_income_from_list = [
    { value: '100000', label: 'INR 1 Lakh' },
    { value: '400000', label: 'INR 4 Lakh' },
    {value: '700000' , label: 'INR 7 Lakh'},
    {value: '1000000' , label: 'INR 10 Lakh'},
    {value: '1500000' , label: 'INR 15 Lakh'},
    {value: '2000000' , label: 'INR 20 Lakh'},
    { value: '3000000', label: 'INR 30 Lakh'},
    {value: '5000000' , label: 'INR 50 Lakh'},
    {value: '7500000' , label: 'INR 75 Lakh'}

  ];
  partner_income_to_list = [
    { value: '200000', label: 'INR 2 Lakh' },
    { value: '700000', label: 'INR 7 Lakh' },
    {value: '1000000' , label: 'INR 10 Lakh'},
    {value: '1500000' , label: 'INR 15 Lakh'},
    {value: '2000000' , label: 'INR 20 Lakh'},
    {value: '3000000' , label: 'INR 30 Lakh'},
    { value: '5000000', label: 'INR 50 Lakh'},
    {value: '7500000' , label: 'INR 75 Lakh'},
    {value: '10000000', label: 'INR 1 Crore'}

  ];

  ddl_partn_annual_from: any;
  ddl_partn_annual_to: any;

  selected_partner_countryID_list = [];
  select_partner_stateID_list = [];
  selected_partner_education_id_list = [];
  selected_partner_occupation_id_list = [];
  // ***********end partner profile fields**************

  list_leisures_activities: string[] = ['Music',
    'Dance',
    'Arts',
    'Media',
    'Theater',
    'Cooking',
    'Volunteering',
    'Gardening',
    'Dining',
    'Modeling',
    'Journalism',
    'Fine arts',
    'Interior design',
    'Fashion design',
    'Indoor sports',
    'Outdoor sports',
    'Water sports',
    'Fitness',
    'Yoga',
    'Spiritual activities',
    'Astrology',
    'Science',
    'Pseudo science',
    'Travel',
    'Writing',
    'Languages',
    'Tailoring',
    'Others'
  ];


  list_leisures_skills: string[] = ['Indian music',
    'Indian music instruments',
    'Indian dance',
    'Western dance',
    'Western music',
    'Western music instrument',
    'Carpentry',
    'Tailoring',
    'Painting',
    'Drawing',
    'Sculpture yoga',
    'Sports',
    'Cooking',
    'Acting',
    'Designer',
    'Actor',
    'Writer',
    'Media skills',
    'Modeling',
    'Gardening',
    'Others'
  ];

  list_visible_toall: string[] = [
    'One',
    'Two',
    'Three'
  ];

  religion_lists: string[] = [
    'Hindu',
    'Muslim',
    'Christian',
    'Sikh',
    'Parsi',
    'Jain',
    'Buddhist',
    'Jewish',
    'No Religion',
    'Spiritual - not religious',
    'Other'
];

  // -----------update my profile start----------
  txt_age = '';
  ddl_profile_created = '';
  profile_list = ['self', 'parent/guardian', 'sibling', 'friend', 'other'];
  txt_date_of_birth = '';
  ddl_marital_status = '';
  marraige_status_list=['never-married','divorced','widow'];
  txt_height = '';
  txt_weight = '';
  ddl_blood_group = '';
  ddl_skin_color = '';
  skincolor_list = ['dark', 'light brown', 'fair', 'very fair'];
  ddl_gothra_gothram = '';
  ddl_rasi = '';
  ddl_nakashtra = '';
  ddl_dosham = '';
  dosham_list = ['yes', 'no', 'dont know'];
  ddl_religion = '';
  txt_mother_tongue = '';
  ddl_caste = '';
  txt_sub_caste = '';
  ddl_physically_challenge = '';
  physical_challange_list = ['no', 'yes'];
  ddl_lang_known = '';
  ddl_food_habbits = '';
  ddl_father_status = '';
  ddl_mother_status = '';
  ddl_family_type = '';
  ddl_brothers = '';
  ddl_sisters = '';
  ddl_brothers_married = '';
  ddl_sisters_married = '';
  ddl_house_hold = '';
  ddl_country_living: any[];
  ddl_state_living: any[];
  ddl_city_living: any[];
  ddl_residency_country = '';
  ddl_residency_state = '';
  ddl_residency_city = '';
  txt_visa_status = '';
  ddl_education = '';
  txt_university = '';
  ddl_employment = '';
  txt_occupation = '';
  ddl_annual_income = '';
  ddl_activities = [];
  ddl_skills = [];
  txt_native = '';
  ddl_relocation = '';
  txt_groom_citizenship = '';
  txt_gender = '';
  selected_education_id: any;
  // -----------update my profile end----------

  // -----------partner profile start----------
  ddl_partn_profile_created = '';
  ddl_partn_marital_status = '';
  ddl_partn_skin_color = '';
  ddl_partn_age_from = '';
  ddl_partn_age_to = '';
  ddl_partn_height_from = '';
  ddl_partn_height_to = '';
  txt_partn_weight = '';
  ddl_partn_gothra_gothram = '';
  ddl_partn_rasi = '';
  ddl_partn_nakashtra = '';
  ddl_partn_dosham = '';
  ddl_partn_religion = '';
  txt_partn_mothertong = '';
  ddl_partn_physically_challenge = '';
  ddl_partn_lang_known = '';
  ddl_partn_family_type = '';
  ddl_partn_country = '';
  ddl_partn_state = '';
  ddl_partn_city = '';
  ddl_partn_residence_country = '';
  ddl_partn_residence_state = '';
  ddl_partn_residence_city = '';
  txt_partn_visastatus = '';
  ddl_partn_education = '';
  ddl_partn_emp = '';
  txt_partn_occupation = '';
  ddl_partn_annual_inc = '';
  ddl_partn_activities = '';
  ddl_partn_skills = '';
  caste_list: any;
  mother_tongue_list: any;
  rasi_list: any;
  nakshatra_list: any;
  gothram_list: any;
  // -----------partner profile end----------

  // -----------contact profile start----------
  txt_mobile_no = '';
  txt_contact_pname = '';
  txt_call_time = '';
  txt_relationship_mem = '';
  txt_display_option = '';
  txt_contact_email = '';
  selectedlanguages: string[] = [];
  languages = [
    { label: 'Telugu', value: 'telugu' },
    { label: 'English', value: 'english' },
    { label: 'Hindi', value: 'hindi' },
    { label: 'French', value: 'french' }
  ];
  religion_list = [
    'Hindu',
    'Muslim',
    'Christian',
    'Sikh',
    'Parsi',
    'Jain',
    'Buddhist',
    'Jewish',
    'Not religious',
    'Other'
  ];
  permisiion_list = [
    { label: 'Visible', value: 'Visible' },
    { label: 'Request', value: 'Request' },
    { label: 'Private', value: 'Private' }

  ];
  rbl_visible: string;

  // -------------file upload preview--------------------
  url1 = '../assets/images/transparent.png';
  url2 = '../assets/images/transparent.png';
  url3 = '../assets/images/transparent.png';

  ngOnInit() {
    debugger;
    // alert(this.userservice.getLoggedinUserId());
    this.displayUserProfileDetails();
    this.displayPartnerDetails();
    this.displayContactDetails();
    this.displayAllCountryList();

    // partner profile methods
    this.displayCasteList();
    this.displayMotherTongueList();
    this.displayRasiList();
    this.displayNakshatraList();
    this.displayGothramList();
    this.displayPartnerEducationList();
    this.displayPartnerOccupationList();
    // this.displayAllPartnerStateList();


  }
  onSelectFile(event: any) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    const i = value.replace('imageUpload', '');
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        if (i == 1) {
          this.url1 = reader.result as string;
        } if (i == 2) {
          this.url2 = reader.result as string;
        } if (i == 3) {
          this.url3 = reader.result as string;
        }
      };
    }
  }
  // ---------------file upload preview----------------

  geteducationId(selected_education_name) {    this.selected_education_id = '';
       for (let i = 0; i < this.parter_education_list.length; i++) {
      if (this.parter_education_list[i].list.includes(selected_education_name) == true) {
        this.selected_education_id = this.parter_education_list[i].id;
      }
    }

  }

  getoccupationId(selected_occupation_name) {    this.selected_occupation_id = '';
       for (let i = 0; i < this.partner_occupation_list.length; i++) {
      if (this.partner_occupation_list[i].list.includes(selected_occupation_name) == true) {
        this.selected_occupation_id = this.partner_occupation_list[i].id;
      }
    }

  }

  getpartnereducationID(selected_partner_education_name) {
    for (let i = 0; i < this.parter_education_list.length; i++) {
      if (this.parter_education_list[i].name == selected_partner_education_name) {
        this.selected_partner_education_id_list.push(this.parter_education_list[i].id);
      }
    }
  }
  getpartneroccupationID(selected_partner_occupation_name) {
    for (let i = 0; i < this.partner_occupation_list.length; i++) {
      if (this.partner_occupation_list[i].name == selected_partner_occupation_name) {
        this.selected_partner_occupation_id_list.push(this.partner_occupation_list[i].id);
      }
    }
  }
  updateprofiledetails(updateProfileForm,userprofiledata) {
    this.clearMessage();
    if (updateProfileForm.valid == false) {
      this.messageService.add({ severity: 'warn', summary: 'Please all required fields', detail: '' });
      return false;
    }
    else {
      
      this.DisplayResidencyCountryID(this.ddl_country_living);
      this.DisplayResidencyStateID(this.ddl_state_living);

      this.DisplayGroomResidencyCountryID(this.ddl_residency_country);
      this.DisplayGroomResidencyStateID(this.ddl_residency_state);

      if (userprofiledata.ddl_annual_income == '') {
        userprofiledata.ddl_annual_income = 700000;
      }
      if (userprofiledata.ddl_country_living == '' || userprofiledata.ddl_residency_country == '') {
        userprofiledata.ddl_country_living = 'India';
        userprofiledata.ddl_residency_country == 'India';
      }
      if (userprofiledata.ddl_state_living == '' || userprofiledata.ddl_residency_state == '') {
        userprofiledata.ddl_state_living = 'Tamil Nadu';
        userprofiledata.ddl_residency_state == 'Tamil Nadu';
      }
      if (userprofiledata.ddl_city_living == '' || userprofiledata.ddl_residency_city == '') {
        userprofiledata.ddl_city_living = 'Chennai';
        userprofiledata.ddl_residency_city == 'Chennai';
      }
      const formdata = {
        userId: this.userservice.getLoggedinUserId(),
        aboutUs: 'Tell about something about me',
        basicInformation: {
          age: userprofiledata.txt_age,
          profileCreatedBy: userprofiledata.ddl_profile_created,
          gender: userprofiledata.txt_gender,
          dateOfBirth: userprofiledata.txt_date_of_birth,
          maritalStatus: userprofiledata.ddl_marital_status,
          height: userprofiledata.txt_height,
          weight: userprofiledata.txt_weight,
          bloodGroup: userprofiledata.ddl_blood_group,
          skinColor: userprofiledata.ddl_skin_color,
          gothram: userprofiledata.ddl_gothra_gothram,
          rasi: userprofiledata.ddl_rasi,
          nakshatra: userprofiledata.ddl_nakashtra,
          dosham: userprofiledata.ddl_dosham,
          religion: userprofiledata.ddl_religion,
          motherTongue: userprofiledata.txt_mother_tongue,
          caste: userprofiledata.ddl_caste,
          subCaste: userprofiledata.txt_sub_caste,
          physicallyChallenged: userprofiledata.ddl_physically_challenge,
          languagesKnown: userprofiledata.ddl_lang_known,
          foodHabits: userprofiledata.ddl_food_habbits
        },
        familyBackground: {
          father: userprofiledata.ddl_father_status,
          mother: userprofiledata.ddl_mother_status,
          familyType: userprofiledata.ddl_family_type,
          native: userprofiledata.txt_native,
          brothers: parseInt(userprofiledata.ddl_brothers),
          brothersMartitalStatus: parseInt(userprofiledata.ddl_brothers_married),
          sisters: parseInt(userprofiledata.ddl_sisters),
          SistersMartitalStatus: parseInt(userprofiledata.ddl_sisters_married),
          houseHold: userprofiledata.ddl_house_hold,
          residenceCountry: userprofiledata.ddl_country_living,
          residenceState: userprofiledata.ddl_state_living,
          residenceCity: userprofiledata.ddl_city_living,
          bridegroomCountry: userprofiledata.ddl_residency_country,
          bridegroomState: userprofiledata.ddl_residency_state,
          bridegroomCity: userprofiledata.ddl_residency_city,
          bgVisaStatus: userprofiledata.txt_visa_status,
          rcoid: this.ddl_country_id,
          rstid: this.ddl_state_id,
          rctid: this.ddl_city_id,
          bgcoid: this.ddl_resendency_country_id,
          bgstid: this.ddl_resendency_state_id,
          bgctid: this.ddl_resendency_city_id,
          bglat: parseFloat(userprofiledata.ddl_residency_city.lat),
          bglong: parseFloat(userprofiledata.ddl_residency_city.longt),
          bridegroomCitizenship: userprofiledata.txt_groom_citizenship,
          relocation: userprofiledata.ddl_relocation,
        },
        professionDetails: {
          education: userprofiledata.ddl_education,
          educationId: this.selected_education_id,
          university: userprofiledata.txt_university,
          employment: userprofiledata.ddl_employment,
          occupation: userprofiledata.txt_occupation,
          occupationId: this.selected_occupation_id,
          annualIncome: userprofiledata.ddl_annual_income
        },
        leisuresSkills: {
          activities: userprofiledata.ddl_activities,
          skills: userprofiledata.ddl_skills,
          others: 'dummy others'
        }
      };
      this.userprofile.UserProfileUpdate(formdata).subscribe(data => {
        if (data.status == 'success') {
          console.log('Submited Data in Update Profile Page : ' + formdata);
          // alert(data.message + "successfully inseted");
          this.messageService.add({ severity: 'success', summary: data.message, detail: '' });
        } else if (data.status == 'failue') {        // alert(data.message);
          this.messageService.add({ severity: 'warn', summary: data.message, detail: '' });
        } else {
          this.messageService.add({ severity: 'warn', summary: data.message, detail: 'Something went wrong while update the my profile data' });
          // alert("Something went wrong while update the my profile data")
        }
      });

    }
  }
   
    

  changePartnerResidencyCountryDropdown() {
    this.is_partner_residency_country_edit = false;
    this.displayAllCountryList();
  }
  changeResidencyCountryDropdown(country_id) {
    this.is_residency_country_edit = false;
    this.DisplayResidencyCountryID(country_id);
  }
  changeResidencyStateDropdown(country_name, state_id) {
    // alert(country_name);
    this.DisplayResidencyCountryID(country_name);
    this.is_residency_state_edit = false;
    this.DisplayResidencyStateID(state_id);

  }
  changeResidencyCityDropdown(state_name) {

    this.is_residency_city_edit = false;
    this.DisplayResidencyStateID(state_name);
  }

  changeGroomCountryDropdown(country_id) {
    this.is_groom_country_edit = false;
    this.DisplayGroomResidencyCountryID(country_id);
  }
  changeGroomStateDropdown(residency_country_name, state_id) {
    this.DisplayGroomResidencyCountryID(residency_country_name);
    this.is_groom_state_edit = false;
    this.DisplayGroomResidencyStateID(state_id);
  }
  changeGroomCityDropdown(residency_state_name) {

    this.is_groom_city_edit = false;
    this.DisplayGroomResidencyStateID(residency_state_name);
  }

  displayUserProfileDetails() {
    debugger;
    this.is_residency_country_edit = true;
                                   this.is_residency_state_edit = true;
                                   this.is_residency_city_edit = true;

                                   this.is_groom_country_edit = true;
                                   this.is_groom_state_edit = true;
                                   this.is_groom_city_edit = true;



                                   this.userprofile.GetUserProfileDataByUserID(this.userservice.getLoggedinUserId()).subscribe(data => {
      this.userprofiledetails = data.response[0];
      this.ddl_profile_created = data.response[0].profileCreatedBy;
      this.txt_gender = data.response[0].gender;
      this.txt_date_of_birth = data.response[0].dateOfBirth;
      this.ddl_marital_status = data.response[0].maritalStatus;
      this.txt_height = data.response[0].height;
      this.txt_weight = data.response[0].weight;
      this.ddl_blood_group = data.response[0].bloodGroup;
      this.ddl_skin_color = data.response[0].skinColor;
      this.ddl_gothra_gothram = data.response[0].gothram;
      this.ddl_rasi = data.response[0].rasi;
      this.ddl_nakashtra = data.response[0].nakshatra;
      this.ddl_dosham = data.response[0].dosham;
      this.ddl_religion = data.response[0].religion;
      this.txt_mother_tongue = data.response[0].motherTongue;
      this.ddl_caste = data.response[0].caste;
      this.txt_sub_caste = data.response[0].subCaste;
      this.ddl_physically_challenge = data.response[0].physicallyChallenged;
      this.ddl_lang_known = data.response[0].languagesKnown;
      this.ddl_food_habbits = data.response[0].foodHabits;
      this.txt_age = data.response[0].age;
      this.ddl_father_status = data.response[0].father;
      this.ddl_mother_status = data.response[0].mother;
      this.ddl_family_type = data.response[0].familyType;
      this.txt_native = data.response[0].native;
      this.ddl_brothers = data.response[0].brothers;
      this.ddl_brothers_married = data.response[0].brothersMartitalStatus;
      this.ddl_sisters = data.response[0].sisters;
      this.ddl_sisters_married = data.response[0].SistersMartitalStatus;
      this.ddl_house_hold = data.response[0].houseHold;
      this.txt_groom_citizenship = data.response[0].bridegroomCitizenship;
      this.ddl_relocation = data.response[0].relocation;
      this.ddl_country_living = data.response[0].residenceCountry;
      this.ddl_state_living = data.response[0].residenceState;
      this.ddl_city_living = data.response[0].residenceCity;
      this.ddl_residency_country = data.response[0].bridegroomCountry;
      this.ddl_residency_state = data.response[0].bridegroomState;
      this.ddl_residency_city = data.response[0].bridegroomCity;

      this.ddl_country_id = data.response[0].rcoid;
      this.ddl_state_id = data.response[0].rstid;
      this.ddl_city_id = data.response[0].rctid;

      this.ddl_resendency_country_id = data.response[0].bgcoid;
      this.ddl_resendency_state_id = data.response[0].bgstid;
      this.ddl_resendency_city_id = data.response[0].bgctid;

      this.txt_visa_status = data.response[0].bgVisaStatus;
      this.ddl_education = data.response[0].education;
      this.txt_university = data.response[0].university;
      this.ddl_employment = data.response[0].employement;
      this.txt_occupation = data.response[0].occupation;
      this.ddl_annual_income = data.response[0].annualIncome;
      this.ddl_activities =  data.response[0].activities;
      this.ddl_skills = data.response[0].skills;
    });
  }


  updatepartnerprofile(partnerprofiledata) {    this.clearMessage();
   // let totalannualincome = partnerprofiledata.ddl_partn_annual_inc.split("-");
                                                const annual_from_salary = parseInt(partnerprofiledata.ddl_partn_annual_from);
                                                const annual_to_salary = parseInt(partnerprofiledata.ddl_partn_annual_to);

                                                if (annual_from_salary > annual_to_salary) {
      this.messageService.add({ severity: 'warn', summary: '"Income To" field Should be greater than "Income From" field', detail: '' });
      return;
    }
    // this.displayAllCountryList();
    // this.DisplayResidencyCountryID(this.ddl_country_id);
    // this.DisplayResidencyStateID(this.ddl_state_id);

    // this.DisplayGroomResidencyCountryID(this.ddl_resendency_country_id);
    // this.DisplayGroomResidencyStateID(this.ddl_resendency_state_id)
                                                const formdata = {
      userId: this.userservice.getLoggedinUserId(),
      ageFrom: parseInt(partnerprofiledata.ddl_partn_age_from),
      ageTo: parseInt(partnerprofiledata.ddl_partn_age_to),
      heightFrom: parseFloat(partnerprofiledata.ddl_partn_height_from),
      heightTo: parseFloat(partnerprofiledata.ddl_partn_height_to),
      maritalStatus: partnerprofiledata.cbl_marital_status,
      religion: partnerprofiledata.ddl_partner_religion,
      appearance: partnerprofiledata.rbl_apperance_status,
      eatingHabbit: partnerprofiledata.cbl_eating_status,
      smokeHabbit: partnerprofiledata.rbl_smoke_status,
      drinkHabbit: partnerprofiledata.rbl_drink_status,
      motherTongue: partnerprofiledata.ddl_partner_lang_known,
      caste: partnerprofiledata.ddl_partner_caste,
      dosham: partnerprofiledata.rbl_dosham_status,
      residenceCountry: partnerprofiledata.ddl_partner_res_country,
      residenceState: partnerprofiledata.ddl_partner_res_state,
      residenceCity: partnerprofiledata.ddl_partner_res_city,
      relocation: partnerprofiledata.rbl_relocation_status,
      education: partnerprofiledata.ddl_partner_education,
      employment: partnerprofiledata.ddl_partner_employment,
      occupation: partnerprofiledata.ddl_partner_occupation,
      incomeFrom: annual_from_salary,
      incomeTo: annual_to_salary
    };
                                                console.log(formdata);
                                                this.userprofile.PartnerProfileUpdate(formdata).subscribe(response => {      if (response.status == 'success') {
        // alert(response.message);
        this.messageService.add({ severity: 'success', summary: response.message, detail: '' });
      } else if (response.status == 'failure') {
        // alert(response.message);
        this.messageService.add({ severity: 'warn', summary: response.message, detail: '' });
      } else {
        this.messageService.add({ severity: 'warn', summary: response.message, detail: 'Something went wrong while partner prefernce data!' });
        // alert("Something went wrong while partner prefernce data!")
      }
    });
  }
  displayPartnerDetails() {
    this.is_partner_residency_country_edit = true;
    this.userprofile.GetPartnerProfileDataByUserID(this.userservice.getLoggedinUserId()).subscribe(data => {
      this.partnerprofiledetails = data.response[0];

      this.ddl_partn_age_from = data.response[0].ageFrom;
      this.ddl_partn_age_to = data.response[0].ageTo;
      this.ddl_partn_height_from = data.response[0].heightFrom;
      this.ddl_partn_height_to = data.response[0].heightTo;
      this.ddl_partner_religion = data.response[0].religion;
      this.ddl_partner_lang_known = data.response[0].motherTongue;
      this.ddl_partner_caste = data.response[0].caste;
      this.ddl_partner_res_country = data.response[0].residenceCountry;
      this.ddl_partner_res_state = data.response[0].residenceState;
      this.ddl_partner_res_city = data.response[0].residenceCity;
      this.ddl_partner_education = data.response[0].education;
      this.ddl_partner_employment = data.response[0].employment;
      this.ddl_partner_occupation = data.response[0].occupation;
      this.rbl_apperance_status = data.response[0].appearance;
      this.cbl_eating_status = data.response[0].eatingHabbit;
      this.cbl_marital_status = data.response[0].maritalStatus;
      this.rbl_smoke_status = data.response[0].smokeHabbit;
      this.rbl_drink_status = data.response[0].drinkHabbit;
      this.rbl_dosham_status = data.response[0].dosham;
      this.rbl_relocation_status = data.response[0].relocation;
      this.ddl_partn_annual_from = data.response[0].incomeFrom;
      this.ddl_partn_annual_to = data.response[0].incomeTo;


      // this.txt_partn_annual_inc = data[0].annual_income_from.toString() + "-" + data[0].annual_income_to.toString();
    });
  }
  displayCasteList() {
    this.userprofile.GetCasteList().subscribe(data => {
      if (data.status == 'success') {
        this.caste_list = data.response;
      }
    });
  }
  displayMotherTongueList() {
    this.userprofile.GetMotherTongueList().subscribe(data => {
      if (data.status == 'success') {
        this.mother_tongue_list = data.response;
      }
    });
  }
  displayRasiList() {
    this.userprofile.GetRasiList().subscribe(data => {
      if (data.status == 'success') {
        this.rasi_list = data.response;
      }
    });
  }
  displayNakshatraList() {
    this.userprofile.GetNakshatraList().subscribe(data => {
      if (data.status == 'success') {
        this.nakshatra_list = data.response;
      }
    });
  }
  displayGothramList() {
    this.userprofile.GetGothramList().subscribe(data => {
      if (data.status == 'success') {
        this.gothram_list = data.response;
      }
    });
    }
  displayPartnerOccupationList() {
    this.userprofile.GetPartnerOccupationList().subscribe(data => {
      if (data.status == 'success') {
        this.partner_occupation_list = data.response;
      }
    });
  }
  displayPartnerEducationList() {
    this.userprofile.GetPartnerEducationList().subscribe(data => {
      if (data.status == 'success') {
        this.parter_education_list = data.response;
      }
    });
  }
  displayAllCountryList() {
    this.userprofile.GetCountryList().subscribe(data => {
      this.all_country_list = data.countryList;
      this.all_groom_country_list = data.countryList;
    });
  }
  displayAllPartnerStateList() {

      this.userprofile.GetPartnerStateListByCountryIDS({
        countryId: this.selected_partner_countryID_list
      }).subscribe(data => {
        if (data.status == 'success') {          this.all_partner_state_list = data.stateList;
        }
       // id,stateName

      });


  }
  displayAllPartnerCityList() {
     this.userprofile.GetPartnerCityListByStateIDS({
      stateId: this.select_partner_stateID_list
      }).subscribe(data => {
        if (data.status == 'success') {          this.all_partner_city_list = data.stateList;
        }

      // id": 38589,
      // "cityName": "Administrative Zone 2",
      // "lat": "13.68513000",
      // "longt": "40.05615000"

    });
  }
  getPartnerCountryID(partner_countryname) {    if (partner_countryname.length == 0) {
      this.selected_partner_countryID_list = [];
      this.all_partner_state_list = [];
    } else {
      for (let i = 0; i < partner_countryname.length; i++) {
      this.selected_partner_countryID_list.push(this.all_country_list.filter(x => x.countryName == partner_countryname[i])[0].id);
    }
      this.displayAllPartnerStateList();

    }

  }
  getPartnerStateID(partner_statename) {    if (partner_statename.length == 0) {
      this.select_partner_stateID_list = [];
      this.all_partner_city_list = [];
    } else {
      for (let i = 0; i < partner_statename.length; i++) {
        this.select_partner_stateID_list.push(this.all_partner_state_list.filter(x => x.stateName == partner_statename[i])[0].id);
      }
      this.displayAllPartnerCityList();

    }


  }


  DisplayResidencyCountryID(countryname) {    this.ddl_country_id = this.all_country_list.find(x => x.countryName == countryname).id;

                                              this.userprofile.GetStateListByCountryID(this.ddl_country_id).subscribe(data => {
      this.all_state_list = data.stateList;

    });

  }
  DisplayResidencyStateID(statename) {    this.ddl_state_id = this.all_state_list.find(x => x.stateName == statename).id;
                                          this.userprofile.GetCityListByStateID(this.ddl_state_id).subscribe(data => {
      this.all_city_list = data.stateList;

    });

  }
  DisplayResidencyCityID(cityname) {    this.ddl_city_id = this.all_city_list.find(x => x.cityName == cityname).id;

  }
  DisplayGroomResidencyCountryID(countryname) {
    this.ddl_resendency_country_id = this.all_groom_country_list.find(x => x.countryName == countryname).id;
    this.userprofile.GetStateListByCountryID(this.ddl_resendency_country_id).subscribe(data => {

      this.all_groom_state_list = data.stateList;

    });

  }
  DisplayGroomResidencyStateID(statename) {
    this.ddl_resendency_state_id = this.all_groom_state_list.find(x => x.stateName == statename).id;
    this.userprofile.GetCityListByStateID( this.ddl_resendency_state_id).subscribe(data => {

      this.all_groom_city_list = data.stateList;

    });

  }
  DisplayGroomResidencyCityID(cityname) {    this.ddl_resendency_city_id = this.all_groom_city_list.find(x => x.cityName == cityname).id;

  }

  contactformsubmit(contactdetails) {

    this.clearMessage();

    const formdata = {
      userId: this.userservice.getLoggedinUserId(),
      parentsContact: this.contactdeatils,
      bridegroomContact: this.contactdeatilsgroom,
      visibleTo: this.rbl_visible
    };
    this.userprofile.ContactFormUpdate(formdata).subscribe(data => {
      if (data.status == 'success') {
        this.messageService.add({ severity: 'success', summary: data.message, detail: '' });
        // alert("inserted successfully");
      } else {
        // alert("inserted successfully");
        this.messageService.add({ severity: 'warn', summary: data.message, detail: '' });
      }
    });
  }
  displayContactDetails() {
    this.userprofile.GetContactDetailsByUserId(this.userservice.getLoggedinUserId()).subscribe(data => {
      this.contactdeatils = data.response[0].parentsContact;
      this.contactdeatilsgroom = data.response[0].bridegroomContact;
      this.rbl_visible = data.response[0].visibleTo;
    });
  }
  clearMessage() {
    this.messageService.clear();
  }

  // setDefaultvalues(totalKeys)
  // {
  //   let i = 0;
  //   for (const property in totalKeys) {

  //     if (totalKeys[property] == "")
  //     {
  //       totalKeys[property] = " ";
  //       this.DefaultData.push( totalKeys[property] = " ")
  //     }
  //    // console.log(`${property}: ${object[property]}`);
  //   }
  // }

  addcontactdetails() {
    if (this.contactdeatils.length < 3) {
      this.contactdeatils.push({
        txt_cnt_mobile_no: '',
        txt_cnt_email: ''
      });
    } else {
      this.clearMessage();
      this.messageService.add({ severity: 'warn', summary: 'we cannot add more than 3 contact numbers!', detail: '' });
    }
  }
  addcontactdetailsgroom() {
    if (this.contactdeatilsgroom.length < 3) {
    this.contactdeatilsgroom.push({
      txt_cnt_mobile_no: '',
      txt_cnt_email: ''
    });
  } else {
    this.clearMessage();
    this.messageService.add({ severity: 'warn', summary: 'we cannot add more than 3 contact numbers!', detail: '' });
  }
  }
  removeContact(i: number) {
    this.contactdeatils.splice(i, 1);
  }
  removeContactgroom(i: number) {
    this.contactdeatilsgroom.splice(i, 1);
  }
  imageUpload(): void {
    const dialogRef = this.dialog.open(ImageuploadComponent, {
      width: '550px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
