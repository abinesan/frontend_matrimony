import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyMatchesComponent } from './dummy-matches.component';

describe('DummyMatchesComponent', () => {
  let component: DummyMatchesComponent;
  let fixture: ComponentFixture<DummyMatchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyMatchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyMatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
