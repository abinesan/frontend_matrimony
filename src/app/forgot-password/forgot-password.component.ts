import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { PasswordService } from '../Service-Layer/password.service';
import { UserService } from '../Service-Layer/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less']
})
export class ForgotPasswordComponent implements OnInit {

  email_or_mobile: any;
  verification_code: any;

  verificationuserId: number;
  constructor(private messageService: MessageService, private router: Router,
              private passwordservice: PasswordService, private userservice: UserService) { }

  ngOnInit() {
  }

  forgotpassword() {
    this.clearMessage();
    const form = {
      emailorMbl: this.email_or_mobile
    };
    this.passwordservice.ForgotPassword(form).subscribe(data => {
      if (data.status == 'success') {
        this.verificationuserId = data.responce.userId;
        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }

    });
  }
  verifypassword() {
    this.clearMessage();
    const formdata = {
      OTP: parseInt(this.verification_code),
      userId: this.verificationuserId
    };
    this.passwordservice.ValidatePassword(formdata).subscribe(data => {
      if (data.status == 'success') {
        this.passwordservice.SetVerificationEmail_UserId(this.verificationuserId);
        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
        this.redirectToPage();
      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }

    });
  }


  clearMessage() {
    this.messageService.clear();
  }

  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/new-password']);
    }, 2000);
  }

}
