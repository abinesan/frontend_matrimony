import { Component, OnInit } from '@angular/core';
import { PasswordService } from 'src/app/Service-Layer/password.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.less']
})
export class NewPasswordComponent implements OnInit {

  txt_user_new_password: any;
  txt_user_conf_password: any;
  constructor(private passwordservice: PasswordService,
              private messageService: MessageService, private router: Router) { }

  ngOnInit() {
  }

  Resetpassword(password, conf_password) {

    this.clearMessage();

    const verification_userid = this.passwordservice.GetVerificationEmail_UserId();

    if (password == conf_password) {
      const formdata = {
        userId: verification_userid,
        password: password
      };
      this.passwordservice.ValidatePassword(formdata).subscribe(data => {
        if (data.status == 'success') {
          this.passwordservice.ResetPassword(formdata);
          this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
          this.redirectToPage();
        } else {
          this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
        }

      });
    } else {

      this.clearMessage();
      this.messageService.add({ severity: 'warn', summary: 'Password and Confirm Password mismatched', detail: ' ' });
    }

  }

  clearMessage() {
    this.messageService.clear();
  }

  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/signup']);
    }, 2000);
  }

}
