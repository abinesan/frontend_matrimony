import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserService } from '../Service-Layer/user.service';
import { AdminGuard } from '../Core/Guards/admin.guard';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.less']
})
export class MainNavComponent {
  admin = true;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, public router: Router,
              public _userservice: UserService, private adminservice: AdminGuard) {}

  logOutUser() {
    if (this.router.url.includes('admin')) {      this.router.navigate(['/admin']);
    } else {
      this._userservice.userLogout();
      this.router.navigate(['/signup']);
    }
  }
}
