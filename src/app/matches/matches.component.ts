import { Component, OnInit } from '@angular/core';
import { MatchesService } from '../Service-Layer/matches.service';
import { UserService } from '../Service-Layer/user.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.less']
})
export class MatchesComponent implements OnInit {
  typesOfSearch: string[] = ['Viewed by me', 'Viewed by others'];
  recentviewed_search_options_list = ['Viewes by me', 'Viewed by others'];
  recent_Page_searchText: any;
  near_page_searchText: any;

  new_matches_filter_list: any;
  new_matches_heading_filter: any;
  new_matches_page_searchText: any;
  all_rencently_viewed_profile_list: any;
  all_matches_profile_list: any;
  all_nearme_matched_profile_list: any;
  todaymatchesdetails: any;
  selectedValues: any;
  ismyprofilevisited = 0;

  viewed_matches_filter: string;

  requestimagepath = 'assets/images/matches/sam.jpg';
  constructor(private router: Router,
              private matchesservice: MatchesService,
              private userservice: UserService,
              private messageService: MessageService) { }

  ngOnInit() {
    this.displayMatchesDetails();
    this.DisplayRecentlyViewedProfileList();
    this.DisplayNearMeMatchedProfileList();
    this.DisplayNewmatchesFilterList();
    this.DisplayAllMatchesProfileList();
  }

  DisplayNewmatchesFilterList() {
    this.matchesservice.GetNewMatchesFilterList().subscribe(data => {

      // this.new_matches_heading_filter = Object.entries(data.response);
      this.new_matches_filter_list = data;
    });
  }

  displayMatchesDetails() {
    this.matchesservice.GetTodayMatchesProfileData(this.userservice.getLoggedinUserId()).subscribe(data => {
      this.todaymatchesdetails = data.myMatchesList;
      // this.todaymatchesdetails[0].pictureRights = "show";
      // if(this.todaymatchesdetails.profile_picture == null){
      //   this.requestimagepath = 'assets/images/matches/request-image.png';
      // }
    });
  }
  viewmoredetails(visited_profileid, current_image_path, current_picture_rights) {
    debugger;
    // changing the value of is my profile visted value if we cliked on more button on UI
    this.ismyprofilevisited = 1;

    this.matchesservice.GetFullProfileViewData({
      senderId: parseInt(this.userservice.getLoggedinUserId()),
      receiverId: parseInt(visited_profileid),
      profile_view_status: this.ismyprofilevisited
    }).subscribe(data => {
      // setting the image
      this.matchesservice.SetCurrentiamge(current_image_path);
      this.matchesservice.SetCurrentImageRights(current_picture_rights);
      //this.router.navigate(['/viewfullprofile'])
      this.router.navigate(['/viewfullprofilewithId/'+parseInt(visited_profileid)])
    });
  }

  DisplayRecentlyViewedProfileList() {
    if (this.viewed_matches_filter == 'viewed_by_others') {
      this.matchesservice.GetRecentlyViewedProfileListByOthers(this.userservice.getLoggedinUserId()).subscribe(data => {
        if (data.status == 'success') {
          this.all_rencently_viewed_profile_list = data.userList;
        }
      });
    } else { this.matchesservice.GetRecentlyViewedProfileListByMe(this.userservice.getLoggedinUserId()).subscribe(data => {
        if (data.status == 'success') {
          this.all_rencently_viewed_profile_list = data.userList;
        }
      });
    }

  }

  FilteredViewedmatchesRecords(index) {    if (this.typesOfSearch[index] == 'Viewed by me') {
      this.viewed_matches_filter = 'viewed_by_me';
      this.DisplayRecentlyViewedProfileList();
    } else if (this.typesOfSearch[index] == 'Viewed by others') {
      this.viewed_matches_filter = 'viewed_by_others';
      this.DisplayRecentlyViewedProfileList();
    }

  }


  DisplayNearMeMatchedProfileList() {
    this.matchesservice.GetNearMeMatchedProfileList(this.userservice.getLoggedinUserId()).subscribe(data => {

      this.all_nearme_matched_profile_list = data.myMatchesList;


    });
  }
  DisplayAllMatchesProfileList() {
    this.matchesservice.GetAllMatchesProfileList(this.userservice.getLoggedinUserId()).subscribe(data => {

      this.all_matches_profile_list = data.myMatchesList;


    });
  }
  Connect(selectedd_profile_id) {    this.clearMessage();
       const formdata = {
      senderId: this.userservice.getLoggedinUserId(),
      receiverId: selectedd_profile_id,
      connectionStatus: 'connect'
    };
       this.matchesservice.SendAccept(formdata).subscribe(data => {
      if (data.status == 'success') {        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });

      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }
    });

  }

  clearMessage() {
    this.messageService.clear();
  }
}
