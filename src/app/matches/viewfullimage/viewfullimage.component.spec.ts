import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewfullimageComponent } from './viewfullimage.component';

describe('ViewfullimageComponent', () => {
  let component: ViewfullimageComponent;
  let fixture: ComponentFixture<ViewfullimageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewfullimageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewfullimageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
