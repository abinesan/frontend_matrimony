import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewfullprofileComponent } from './viewfullprofile.component';

describe('ViewfullprofileComponent', () => {
  let component: ViewfullprofileComponent;
  let fixture: ComponentFixture<ViewfullprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewfullprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewfullprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
