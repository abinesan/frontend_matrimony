import { Component, OnInit } from '@angular/core';
import { UserService } from '../Service-Layer/user.service';
import { PasswordService } from '../Service-Layer/password.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  txt_old_password: any;
  txt_user_actual_password: any;
  txt_user_conf_password: any;
  constructor(private userservice: UserService, private passwordservice: PasswordService,
              private messageService: MessageService, private router: Router) { }

  ngOnInit() {
  }

  changePassword(formdata) {
    this.clearMessage();
    const form = {
      userId: this.userservice.getLoggedinUserId(),
      oldPassword: formdata.txt_user_old_password,
      newPassword: formdata.txt_user_password
    };
    this.passwordservice.ChangePassword(form).subscribe(data => {
      if (data.status == 'success') {
        this.messageService.add({ severity: 'success', summary: data.message, detail: ' ' });
        this.redirectToPage();
      } else {
        this.messageService.add({ severity: 'warn', summary: data.message, detail: ' ' });
      }

    });
  }

  clearMessage() {
    this.messageService.clear();
  }

  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/signup']);
    }, 2000);
  }

}
