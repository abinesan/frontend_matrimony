import {Component,  OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit  {
  constructor(private _formBuilder: FormBuilder) {}
//  slider

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;



  status = new FormControl();
  statusList1: string[] = ['Never married', 'Divorced', 'Widowed', 'Awaiting Divorce', 'Annulled'];

  religion = new FormControl();
  religionList: string[] = ['Hindu', 'Christian', 'Jain', 'Muslim', 'Parsi', 'Buddhist', 'Sikh', 'Jewish', 'No religion', 'Other'];

  motherTongue = new FormControl();
  motherTongueList: string[] = ['Hindi', 'Kannada', 'Tamil', 'Telugu', 'Bengali', 'Gujarati', 'Engilish', 'Marwari', 'Punjabi', 'Urdu'];


  country = new FormControl();
  countryList: string[] = ['India', 'Usa', 'Malaysia', 'Sri Lanka', 'Canada', 'Australia', 'Hong Kong SAR', 'Qatar', 'Singapore', 'United Kingdom'];

  State = new FormControl();
  StateList: string[] = ['Tamil Nadu', 'Karnataka', 'Maharashtra', 'Pondicherry', 'Andhra Pradesh', 'Puducherry', 'Delhi-NCR', 'Kerala', 'Telangana', 'Delhi', 'Andaman & Nicobar', 'Haryana', 'Madhya Pradesh', 'Orissa', 'Rajasthan', 'Other'];

  education = new FormControl();
  educationList: string[] = ['Masters', 'bachelors / Undergraduate', 'Associate Degree / Diploma', 'Doctorate', 'High School and below'];


  working = new FormControl();
  workingList: string[] = ['Private Company', 'Government / Public Sector', 'Defense / Civil Services', 'Business / Self Employed', 'Not Working'];


  profession = new FormControl();
  professionList: string[] = ['Accounting', 'Banking', 'FinanceAdvertising', 'Media', 'EntertainmentAgricultureAdministration', 'HRAirline', 'DesignArtists ', 'Fashion', 'Customer SupportCivil Services', 'Not Working', 'Hospitality', 'IT', 'Software Engineering', 'Hotel', 'BPO, KPO', 'Medical', 'Healthcare', 'Marketing', 'OthersSales'];

  age = new FormControl();
  ageList: string[] = ['18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37'];

  age1 = new FormControl();
  ageList1: string[] = ['18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37'];

  step = 0;
//  slider
  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'L';
    }

    return value;
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
}
