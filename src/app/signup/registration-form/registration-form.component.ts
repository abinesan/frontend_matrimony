import { Component, OnInit } from '@angular/core';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserprofileService } from 'src/app/Service-Layer/userprofile.service';
import { UserService } from 'src/app/Service-Layer/user.service';
import { Message } from 'primeng//api';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { OTPService } from 'src/app/Service-Layer/otp.service';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.less'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class RegistrationFormComponent implements OnInit {

  // firstFormGroup: FormGroup;
  // secondFormGroup: FormGroup;
  isLinear: any;
  mother_tongue_list: any;
  caste_list: any;
  gothram_list: any;

  all_groom_country_list: any;

  // ******************* first form fields start here **********************

  Apperance_list = [
    'Fair', 'Whitish', 'Dark', 'Doesn\'t Matter'
  ];

  day_list = [];
  month_list = [{
    value: '01',
    label: 'January'
  },
  {
    value: '02',
    label: 'Febuary'
    },
    {
      value: '03',
      label: 'March'
    },
    {
      value: '04',
      label: 'April'
    },
    {
      value: '05',
      label: 'May'
    },
    {
      value: '06',
      label: 'June'
    },
    {
      value: '07',
      label: 'July'
    },
    {
      value: '08',
      label: 'August'
    },
    {
      value: '09',
      label: 'September'
    },
    {
      value: '10',
      label: 'October'
    },
    {
      value: '11',
      label: 'November'
    },
    {
      value: '12',
      label: 'December'
    }

  ];

  year_list = [];
  txt_user_dob: string;
  ddl_user_height: any;
  ddl_user_appearence: string;
  ddl_user_religion: string;
  ddl_user_mothertongue: string;
  ddl_user_caste: string;

  txt_user_subcaste: string;
  ddl_user_gothram: string;
  ddl_user_dosham: string;


  // ******************* first form fields ends here **********************

  // ******************* second form fields start here **********************
  all_country_list: any;
  all_state_list: any;
  all_city_list: any;
  ddl_user_country: any;
  ddl_user_state: any;
  ddl_user_city: any;
  txt_user_citizan: string;
  txt_user_visastatus: string;
  // ******************* second form fields ends here **********************


  // **************** third form fields start here *************************
  education_lists: any;
  occupation_list: any;
  user_income_list = [
    { value: '100000-200000', label: 'INR 1 Lakh to 2 Lakh' },
    { value: '400000-700000', label: 'INR 4 Lakh to 7 Lakh' },
    {value: '700000-100000' , label: 'INR 7 Lakh to 10 Lakh'},
    {value: '1000000-1500000' , label: 'INR 10 Lakh to 15 Lakh'},
    {value: '1500000-2000000' , label: 'INR 15 Lakh to 20 Lakh'},
    {value: '2000000-3000000' , label: 'INR 20 Lakh to 30 Lakh'},
    { value: '3000000-5000000', label: 'INR 30 Lakh to 50 Lakh'},
    {value: '5000000-7500000' , label: 'INR 50 Lakh to 75 Lakh'},
    {value: '7500000-10000000' , label: 'INR 75 Lakh to 1 Crore'}

  ];
  ddl_user_education: string;
  ddl_user_employment: string;
  ddl_user_occupation: string;
  ddl_user_income: string;


  // **************** third form fields Ends here *************************

  constructor(private _formBuilder: FormBuilder,
              private userprofileservice: UserprofileService,
              private userservice: UserService,
              private messageService: MessageService,
              private router: Router,
              private otpservice: OTPService
    ) { }

  religion_lists: string[] = [
    'Hindu',
    'Muslim',
    'Christian',
    'Sikh',
    'Parsi',
    'Jain',
    'Buddhist',
    'Jewish',
    'No Religion',
    'Spiritual - not religious',
    'Other'
  ];


  ngOnInit() {

    const current_year = new Date().getFullYear();
    // day list
    for (let day_num = 1; day_num <= 31; day_num++) {
      this.day_list.push(day_num);
    }
    // year list
    for (let start_year = 1930; start_year <= current_year; start_year++) {
      this.year_list.push(start_year);
    }

    // this.firstFormGroup = this._formBuilder.group({
    //   firstCtrl: ['', Validators.required]
    // });
    // this.secondFormGroup = this._formBuilder.group({
    //   secondCtrl: ['', Validators.required]
    // });
    this.displayMotherTongueList();
    this.displayCasteList();
    this.displayGothramList();
    this.displayPartnerOccupationList();
    this.displayPartnerEducationList();
    this.displayAllCountryList();
  }
  displayAllCountryList() {
    this.userprofileservice.GetCountryList().subscribe(data => {
      this.all_country_list = data.countryList;
      this.all_groom_country_list = data.countryList;
    });
  }

  displayMotherTongueList() {
    this.userprofileservice.GetMotherTongueList().subscribe(data => {
      if (data.status == 'success') {
        this.mother_tongue_list = data.response;
      }
    });
  }
  displayCasteList() {
    this.userprofileservice.GetCasteList().subscribe(data => {
      if (data.status == 'success') {
        this.caste_list = data.response;
      }
    });
  }
  displayGothramList() {
    this.userprofileservice.GetGothramList().subscribe(data => {
      if (data.status == 'success') {
        this.gothram_list = data.response;
      }
    });
    }
    displayPartnerOccupationList() {
    this.userprofileservice.GetPartnerOccupationList().subscribe(data => {
      if (data.status == 'success') {
        this.occupation_list = data.response;
      }
    });
  }
  displayPartnerEducationList() {
    this.userprofileservice.GetPartnerEducationList().subscribe(data => {
      if (data.status == 'success') {
        this.education_lists = data.response;
      }
    });
  }
  DisplayResidencyCountryID(countryname) {    const country_id = this.all_country_list.find(x => x.countryName == countryname).id;

                                              this.userprofileservice.GetStateListByCountryID(country_id).subscribe(data => {
      this.all_state_list = data.stateList;

    });

  }
  DisplayResidencyStateID(statename) {    const state_id = this.all_state_list.find(x => x.stateName == statename).id;
                                          this.userprofileservice.GetCityListByStateID(state_id).subscribe(data => {
      this.all_city_list = data.stateList;

    });

  }

  SendOTP(firstformdata, secondformdata, thirdformdata) {    this.clearMessage();


       const basic_userdata = this.userservice.getUserbasicDetails();
       const firstform_userdata = firstformdata;
       const secondform_userdata = secondformdata;
       const thirdform_userdata = thirdformdata;

       const final_userdata = {
      ...basic_userdata,
      ...firstform_userdata,
      ...secondform_userdata,
      ...thirdform_userdata
    };

       const user_mobilenumber =
    // sending all this four forms data to register page
    this.userservice.setUserRegistrationPageDetails(final_userdata);

    // logic to send the OTP and then store the session id globally to verify the OTP later
       const mobileno = this.userservice.getUserMobileNumber();
       this.otpservice.GenarateOTP(mobileno).subscribe(data => {
      if (data.Status == 'Success') {        const sessionid = data.Details;
                                             this.otpservice.SetCurrentValidSessionID(sessionid);
                                             this.messageService.add({ severity: 'success', summary: 'OTP has been sent to your Mobile Number', detail: ' ' });
                                             this.redirectToPage();
      } else {
        this.messageService.add({ severity: 'warn', summary: data.message , detail: ' ' });
      }
    });


  }

  clearMessage() {
    this.messageService.clear();
  }
  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/register']);
    }, 3000);
  }



}
