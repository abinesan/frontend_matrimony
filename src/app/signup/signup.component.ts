import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../Service-Layer/user.service';
import { Router } from '@angular/router';
import { ApplicationConstants } from '../Core/Models/application-constants';
import { UserLogin } from '../Core/Models/user-login';

import { Message } from 'primeng//api';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.less']
})
export class SignupComponent implements OnInit {

  // userLogin: UserLogin;
  txt_user_email: any = '';
  txt_user_password: any = '';

  isForgot_Page_Visible = false;
  isLogin_Page_Visible = true;
  msgs: any;
  constructor(
    private userserservice: UserService,
    private router: Router,
    private messageService: MessageService) { }


  ngOnInit() {
  }


  gotoForgotpage() {
    this.isForgot_Page_Visible = true;
    if (this.isForgot_Page_Visible == true) {
      this.isLogin_Page_Visible = false;
    }

  }

  newUserLogin(formdata) {    this.clearMessage();
                              this.userserservice.userLogin({
      email: formdata.txt_user_email,
      password: formdata.txt_user_password
    }).subscribe(response => {
      if (response.status == 'success') {        this.messageService.add({ severity: 'success', summary: response.message, detail: ' ' });
                                                 localStorage.setItem('accessToken', response.userDetail.access_token);
                                                 localStorage.setItem('refreshToken', response.userDetail.reference_token);

        // setting Accesstoken for the Interceptor to add in Request Header for each Request
                                                 this.userserservice.setAuthenticationTokens(response.userDetail.access_token, response.userDetail.reference_token, response.userId);
                                                 this.userserservice.setLoggedinUserId(response.userId);
                                                 this.userserservice.setUserRole('user');
                                                 this.redirectToPage();

      } else if (response.status == 'failure') {        console.log('Authentication Failure');
                                                      this.messageService.add({ severity: 'warn', summary: response.message, detail: ' ' });
      }

    },
      err => {
        console.log('Sommething went wrong while login');
        this.messageService.add({ severity: 'error', summary: 'Sommething went wrong while login', detail: err });
        console.log(err);

      });
  }

  clearMessage() {
    this.messageService.clear();
  }
  redirectToPage() {
    setTimeout(() => {
      this.router.navigate(['/dashboard']);
    }, 2000);
  }

}
