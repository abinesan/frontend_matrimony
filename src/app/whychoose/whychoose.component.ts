import { Component, OnInit } from '@angular/core';
interface FeaturePrograms {
  feName: string;
  feredirectTo: string;
  feIcon: string;
  fecontent: string;
}
@Component({
  selector: 'app-whychoose',
  templateUrl: './whychoose.component.html',
  styleUrls: ['./whychoose.component.less']
})
export class WhychooseComponent implements OnInit {
  FeatureProgramsData: FeaturePrograms[] = [{
    feName: 'Security',
    feredirectTo: '#',
    feIcon: 'assets/images/rings.png',
    fecontent: 'Our service will give a secure search experience with trust, honor, and respect. The comprehensive range of services will help to find a precise match at one-stop.'
  },
  {
    feName: 'Quality',
    feredirectTo: '#',
    feIcon: 'assets/images/rings.png',
    fecontent: 'The exclusive search for your need will give upscale and meaningful experience and weed out random, multiple, unsuitable, and irrelevant profiles.'
  },
  {
    feName: 'Satisfaction',
    feredirectTo: '#',
    feIcon: 'assets/images/rings.png',
    fecontent: 'The highest quality in our responses and services will give 100% satisfaction and will reduce the pressure of profile search in the busy world.'
  }
  ];
  constructor() { }

  ngOnInit() {
  }

}
